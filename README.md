## DinO

A configuration based dependency injection and IoC framework for NodeJS.

On startup DinO will take care of defining the application context and inject the component defined into the dependent components.
DinO will load configuration from command line environment variables and if defined from a JSON file, the path for the configuration file can be defined
using one of:

- **dino.config.path** from command line, i.e. --dino.config.path /config/app.json
- **DINO_CONFIG_PATH** as a environment variable, i.e. DINO_CONFIG_PATH=/config/app.json node index.js

in both cases the configuration path should be provided relatively to the root directory of your project DiNo will take care of normalizing the path
and lookup the configuration file.

## How it works

DinO is based on [awilix](https://www.npmjs.com/package/awilix) dependency injection framework and registers to the container created all components that have been defined on the _dino.yml_ context configuration file and/or that are automatically resolved via context scan.

Configuration path is provided to the framework at start-up and the path must be relative to the root of the your project.

Context scan is automatically enabled when no configuration is defined and when the contextScan configuration variable is set to true on the dino.yml configuration file. When contextScan is enabled DiNo will look for configuration classes as well, that is configuration classes allow you to define configuration for your application in a programmatic way, the configuration class is designed so that only one object can be returned that is to enforce the Single Responsibility principle and drive the creation of different configuration class for different aspects of the application. An example of implemented configuration class can be found on the **contextscan** example on the **examples** folder.

Context scan root is defined as:

- The **{root of the project}/src/main/node** folder if not environment variable or configuration are provided, this is the default
- using the DINO_CONTEXT_ROOT environment variable a path relative to the root of the project can be defined, the context scan will then take place recursively from this directory.
- using the contextRoot configuration variable, this is provided as pert of the dino.yml configuration file, a path relative to the root of the project can be defined, the context scan will then take place recursively from this directory.

Configuration is read and all defined components registered for injection with the configured scope.

Due to the default context scan algorithm DinO expects the following folder structure:

```
root
  |_ main.js (the entrypoint for your application)
  |_ src
      |_ main
          |_ node (here you can define your application sources)
      |_ test
          |_ node (here you can define your application test sources)
```

The test section is defined for coherence.

As shown on the provided examples DinO can be configured providing the above-mentioned context configuration variables.

DinO expose the configuration defined via command-line, environment variables or file via the _environment_ component instance, that is just reference it in your constructor and you will be able to access your configuration as

```javascript
let value = environment.get('your key');
```

DinO user [nconf](https://github.com/indexzero/nconf) in the background and the _environment_ component is just an nconf instance.

### Examples

Following example shows hot to get DinO up and running, the full code is available on the examples folder.

```javascript
require('dino').run('example/dino.yml').resolve('testComponent').printEnvironment();
```

that is all is required.

The examples directory contains different scenario on which DinO can be employed:

1. Configuration based scenario
2. Context Scan based scenario
3. Mixed Context Scan and Configuration based scenario

### Lifecycle

DinO provide the following lifecycle hooks:

- Post-Construct, each class that extends Component automatically inherit a postConstruct method, the method is invoked following creation of the class and should be used to initialize the internal structures of the component. This should be preferred to the constructor which should be used to assign the injected dependencies only.
- Pre-Destroy, each class that extends Component automatically inherit a preDestroy method, the method is invoked before the exit of the application and should be used to release all resources used by the component, i.e. stop receiving requests from a server.

#### Example

```javascript
class MyComponent extends Component {
  constructor({ dependency }) {
    this.dependency = dependency;
  }

  postConstruct() {
    // logic here
  }

  preDestroy() {
    // logic here
  }
}
```

### Lazy Loading

DinO support lazy loading of components for both scan and configuration based context loading approaches, in order to enable lazy loading: 1) override the static method `lazy()` on your concrete component class implementation or 2) configure your component adding the property `lazy = true` on your context YAML file configuration.

Lazy loaded component will honour the defined lifecycle if any.

### Transactions

See https://gitlab.com/codesketch/dino/-/wikis/Transactions

### Caching

See https://gitlab.com/codesketch/dino/-/wikis/Caching

### Sponsors

- [Applaudart](https://www.applaudart.com)
