const fs = require('fs');

const versionsPath = `${__dirname}/../public/resources/config/versions.json`;

const versions = require(versionsPath);
const packageJson = require(`${__dirname}/../package.json`);

if(versions.find(version => version.id == packageJson.version)) {
  process.exit(0);
}
versions.map(version => {
  if(version.latest) {
    version.latest = false;
  }
  return version;
});
versions.unshift({
  'id': packageJson.version,
  'latest': true
});

fs.writeFileSync(versionsPath, JSON.stringify(versions));