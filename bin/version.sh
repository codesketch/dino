#!/bin/bash
set -e

VERSION=${1:-patch}

npm run test
npm run lint
npm run build
npm run jsdoc
git add -A .
git commit -am 'release new ${VERSION} version'
npm version ${VERSION}