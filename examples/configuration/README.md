## Configuration based DiNo example

This example shows how to configure DiNo and leverage ot configuration to define the components that will be injected on the application context.

A test component is created and injected on the application context to be resolved. When created the component will be injected with the environment configuration resolved by DiNo and the fs module.

### Run it

```bash
node examples/configuration/main.js
```
