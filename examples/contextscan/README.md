## Context Scan based DiNo example
This example shows how to leverage DiNo's context scan to discover and define the components that will be injected on the appliction context.

For this to work the component need to extend DiNo abstract **Component** class, this will tell DiNo IoC container to register the class and make it available.

A test component is created and injected on the application context to be resolved. When created the component will be injected with the environment configuration resolved by DiNo.

### Run it
```bash
DINO_CONTEXT_ROOT=examples/contextscan node examples/contextscan/main.js
```

This example demonstrate as well how to disambiguate or limit when necessary the path scanned by DiNo, that is, if not context root is provided via configuration file or environment variable the context is scanned from the root, node_modules, nyc_output, coverage folders are excluded by defualt. Providing the context root tells DiNo to start scaning from the provided directory, the directory is considered relative to the project root.