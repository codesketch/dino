/* eslint-disable no-console */
//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { ErrorHandler } from '../../../src/main/node/model/error.handler';

class BaseErrorHandler extends ErrorHandler {
  /**
   * Extend the default error handler
   * @param {Error} error the error
   * @param {ErrorType} type the error type
   */
  handle(error, type) {
    console.log(`This is a simple extension of a base error handler I got an ${type} with message: ${error.message}`);
  }
}
export = BaseErrorHandler;
