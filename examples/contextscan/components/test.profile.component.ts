/* eslint-disable no-console */
//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Component } from '../../../src/main/node/model/component';

class TestProfileComponent extends Component {
  private readonly environment: any;
  private readonly fs: any;

  constructor({ environment, fs }) {
    super();
    this.environment = environment;
    this.fs = fs;
  }

  printEnvironment() {
    console.log(JSON.stringify(this.environment));
    console.log('>> do I have fs as well? ', undefined != this.fs);
  }

  static profile() {
    return 'test';
  }
}

module.exports = TestProfileComponent;
