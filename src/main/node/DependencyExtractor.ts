// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const STRIP_COMMENTS_REGEX = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm
const ARGUMENT_NAMES_REGEX = /([^\s,]+)/g
const STRIP_PROXY_REGEX = /[\s{\s}\s]/gm

export class DependencyExtractor {
  /**
   * Extract dependencies for the provided class if any
   * @param {Object} clazz the class the dependencies should be extracted for
   *
   * @returns {Array}
   * @public
   */
  public extractDependenciesFor(clazz: any): string[] {
    let currentLevel = clazz
    const answer: string[] = []
    let definition: string = currentLevel.toString().replace(STRIP_COMMENTS_REGEX, '')
    while (definition.includes('extends') && !definition.includes('constructor')) {
      // update chain level
      // eslint-disable-next-line no-proto
      currentLevel = currentLevel.__proto__
      // get new definition
      definition = currentLevel.toString().replace(STRIP_COMMENTS_REGEX, '')
    }
    while (definition.includes('constructor')) {
      answer.push(...this.getArguments(definition))
      // update chain level
      // eslint-disable-next-line no-proto
      currentLevel = currentLevel.__proto__
      // get new definition
      definition = currentLevel.toString().replace(STRIP_COMMENTS_REGEX, '')
    }
    return answer
  }

  getArguments(definition: string): string[] {
    const argumentList: string = definition.slice(definition.indexOf('(') + 1, definition.indexOf(')'))
    return argumentList.replace(STRIP_PROXY_REGEX, '').match(ARGUMENT_NAMES_REGEX) ?? []
  }
}
