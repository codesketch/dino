//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import winston from 'winston'

const errorStackFormat = winston.format((info) => {
  if (info instanceof Error) {
    return Object.assign({}, info, {
      stack: info.stack,
      message: info.message
    })
  }
  return info
})

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class Logger {
  private static readonly concreteLogger = winston.createLogger({
    level: process.env.LOG_LEVEL ?? 'info',
    transports: [new winston.transports.Console()],
    exitOnError: false,
    format: winston.format.combine(errorStackFormat(), winston.format.timestamp(), winston.format.json())
  })

  public static error(message: string, ...meta: any[]): Logger {
    Logger.concreteLogger.error(message, meta)
    return this
  }

  public static warn(message: string, ...meta: any[]): Logger {
    Logger.concreteLogger.warn(message, meta)
    return this
  }

  public static info(message: string, ...meta: any[]): Logger {
    Logger.concreteLogger.info(message, meta)
    return this
  }

  public static debug(message: string, ...meta: any[]): Logger {
    Logger.concreteLogger.debug(message, meta)
    return this
  }

  public static silly(message: string, ...meta: any[]): Logger {
    Logger.concreteLogger.silly(message, meta)
    return this
  }
}
