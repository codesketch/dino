//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { asValue, createContainer, type AwilixContainer } from 'awilix'
import camelCase from 'camel-case'
import { Logger } from '../Logger'
import { EnvironmentConfigurationBuilder } from '../environment/environment.configuration.builder'
import { Helper } from '../helper/helper'
import { ObjectHelper } from '../helper/object.helper'
import { PromiseHelper } from '../helper/promise.helper'
import { ComponentDescriptor } from '../model/ComponentDescriptor'
import { Component } from '../model/component'
import { Configuration } from '../model/configuration'
import { type EnvironmentConfiguration } from '../model/environment.configuration'
import { ErrorHandler } from '../model/error.handler'
import { Injectable } from '../model/injectable'
import { Resolver } from '../model/resolver'
import { Scope } from '../model/scope'
import { ApplicationContextBuilder } from './ApplicationContextBuilder'
import { ContextConfigurationProvider } from './ContextConfigurationProvider'
import { ContextScope } from './ContextScope'

/**
 * Define the application context
 *
 * @public
 */
export class ApplicationContext {
  private loaded: boolean
  private contextConfigurationProvider: ContextConfigurationProvider | undefined
  private readonly executedLifecycles: string[]
  private container!: AwilixContainer

  /**
   * Create a new application context.
   *
   * @constructor
   */
  constructor() {
    this.loaded = false
    this.contextConfigurationProvider = undefined
    this.executedLifecycles = []
  }

  public createScope(): ContextScope {
    return new ContextScope(this.container.createScope(), this)
  }

  /**
   * Try resolve the requested dependency, if not resolved try require the dependency and if found
   * registers it on the application context otherwise throw error.
   *
   * > **Will not take dependency graph into account!**
   *
   * @param {String} name the name of the dependency to require
   * @throws {Error} when not able to resolve the dependency
   *
   * @public
   */
  public require(name: string): any {
    if (this.contextConfigurationProvider === undefined) {
      throw Error('context configuration provider is not defined!!!')
    }
    const _name = Helper.isPath(name) ? Helper.fileNameFromPath(name) : name
    let answer = this.container.resolve(_name, { allowUnregistered: true })
    if (answer === undefined) {
      let dependency: any
      try {
        dependency = require(name)
      } catch (error) {
        const contextRoots = this.contextConfigurationProvider.getContextRoots()
        for (let i = 0; i < contextRoots.length; i++) {
          const source = Helper.findSource(contextRoots[i], name)
          if (source !== undefined) {
            dependency = require(source)
            break
          }
        }
      }
      let scope = Scope.SINGLETON
      let resolver = Resolver.VALUE
      let lazy = false
      const dependsOn = []
      if (ObjectHelper.instanceOf(dependency, Injectable)) {
        Logger.debug('injecting new component from require')
        scope = Helper.asLifetime((dependency as Injectable).scope())
        resolver = Resolver.CLASS
        lazy = (dependency as Injectable).lazy() ?? false
      }
      // eslint-disable-next-line @typescript-eslint/ban-types
      answer = this.register(_name, ComponentDescriptor.create(name, dependency as string | {} | Injectable, scope, resolver, lazy, dependsOn))
    }
    return answer
  }

  /**
   * Resolve a component from this context and return a reference.
   * @param {String} component the name of the component to resolve
   *
   * @public`
   */
  public resolve(component: string): any {
    const name = camelCase(component)
    const instance = this.container.resolve(name, { allowUnregistered: false })
    const lifetime = this.container.registrations[name] !== undefined ? this.container.registrations[name].lifetime : 'UNKNOWN'
    this.executeLifecycleIfNeeded(instance, lifetime === 'TRANSIENT')
    return instance
  }

  /**
   * Resolves all components of a particular type. This is a fairly expensive method
   * as such it should be used with caution.
   * The current implementation checks the context cache and the registrations skipping already cached
   * instances, the drawback is that unwanted registration are resolved potentially ahead of time.
   *
   * While work is ongoing to resolve the drawback a suitable workaround might be to instantiate a factory via
   * configuration, this creates a manual overhead but might avoid issues.
   *
   * @param {Function} type the type to resolve
   * @param [onlyCached=false] allows to define is only cached injectables will be resolved, defaults to false meaning that
   * all registration will be scanned and forcibly loaded.
   * @returns {Array<any>}
   *
   * @public
   */
  public resolveAll(type: any, onlyCached: boolean = false): any[] {
    const answer: any[] = []
    const cacheEntries = Array.from(this.container.cache.keys())
    const injectablesFilter = (key: string | symbol): boolean =>
      !key.toString().startsWith('dinoConcreteInstanceForLazy_') && !['applicationContext', 'environment'].includes(key.toString())
    cacheEntries.filter(injectablesFilter).forEach((key) => {
      const obj = this.container.cache.get(key)
      if (obj !== undefined) {
        const target = obj.value
        if (ObjectHelper.instanceOf(target, type)) {
          this.executeLifecycleIfNeeded(target, obj.resolver.lifetime === 'TRANSIENT')
          answer.push(target)
        }
      }
    })
    if (!onlyCached) {
      Object.keys(this.container.registrations)
        .filter(injectablesFilter)
        .forEach((key) => {
          if (!cacheEntries.includes(key)) {
            // try resolve only if a cached instance has not been found
            const resolver = this.container.registrations[key]
            const obj = resolver.resolve(this.container)
            if (ObjectHelper.instanceOf(obj, type) || ObjectHelper.instanceOf(obj.constructor, type)) {
              this.executeLifecycleIfNeeded(obj, resolver.lifetime === 'TRANSIENT')
              answer.push(obj)
            }
          }
        })
    }
    return answer
  }

  /**
   * Add, register, a component on this context. This is a replacement of the register function
   * that will be deprecated on future releases.
   *
   * The main difference is that it uses the name provided via ComponentDescriptor avoiding the possibility
   * of double naming for injectables.
   *
   * @param {ComponentDescriptor} componentDescriptor the component descriptor able to resolve this component
   * @returns {any} the registered instance
   *
   * @public
   */
  public add(componentDescriptor: ComponentDescriptor): any {
    return this.register(componentDescriptor.getName(), componentDescriptor)
  }

  /**
   * Register a component on this context
   * @param {String} name the component name
   * @param {ComponentDescriptor} resolver the component descriptor able to resolve this component
   * @returns {any} the registered instance
   *
   * @public
   * @deprecated use `ApplicationContext#add` instead
   */
  public register(name: string, resolver: ComponentDescriptor): any {
    const _name = camelCase(name)
    Logger.debug(`** register ${_name} on context`)
    let instance = this.container.resolve(_name, { allowUnregistered: true })
    if (instance === undefined) {
      // need to register and resolve instead of build as this will respect awilix
      // internal structures related to lifecycle.
      instance = this.container.register(_name, resolver.resolve(this)).resolve(_name)
    }
    if (this.executedLifecycles[_name] === undefined) {
      this.executeLifecycleIfNeeded(instance, true)
      this.executedLifecycles.push(_name)
    }
    return instance
  }

  /**
   * Try to resolve an injectable instance
   *
   * @param {String} name the instance name
   * @param {Boolean} doExecuteLifecycle a flag indicating if the lifecycle should be executed
   * @returns {Injectable} the resolved instance
   *
   * @private
   */
  protected tryResolveInstance(name: string, doExecuteLifecycle: boolean): Injectable | undefined {
    try {
      const instance: Injectable = this.container.resolve(name)
      this.executeLifecycleIfNeeded(instance, doExecuteLifecycle)
      return instance
    } catch (e) {
      //
    }
    return undefined
  }

  /**
   * Execute an component lifecycle if required
   * @param {Injectable} instance the instance to execute the lifecycle for
   * @param {Boolean} doExecuteLifecycle a flag indicating fif the lifecycle should be executed
   * @returns {void}
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  protected executeLifecycleIfNeeded(instance: Injectable | any, doExecuteLifecycle: boolean): void {
    if (!doExecuteLifecycle) {
      return
    }
    if (instance instanceof Component || instance instanceof ErrorHandler) {
      const answer: Promise<void> | void = instance.postConstruct()
      PromiseHelper.handleAsPromiseIfRequired(answer, (cd: ComponentDescriptor) => cd !== undefined && this.add(cd))
    }
    if (instance instanceof Configuration) {
      const componentDescriptor: ComponentDescriptor | Promise<ComponentDescriptor> = instance.configure()
      PromiseHelper.handleAsPromiseIfRequired(componentDescriptor, (cd: ComponentDescriptor) => cd !== undefined && this.add(cd))
    }
    if (instance instanceof ErrorHandler) {
      instance.postConstruct()
      Logger.debug('global error handler registered')
    }
  }

  /**
   * Load the application context
   * @param {String} config path, relative to the root directory of the project
   * of the application context configuration file
   * @returns {ApplicationContext} the application context instance
   *
   * @public
   */
  public async load(config?: string): Promise<void> {
    const start = Date.now()
    try {
      if (this.loaded) {
        throw new Error('application context is already loaded')
      }
      const environment: EnvironmentConfiguration = EnvironmentConfigurationBuilder.load()

      this.contextConfigurationProvider = ContextConfigurationProvider.create(config, this.getActiveProfiles(environment))
      this.container = createContainer({ injectionMode: this.contextConfigurationProvider.mode() })
      this.container.register('applicationContext', asValue(this))
      this.container.register('environment', asValue(environment))

      const applicationContextBuilder = new ApplicationContextBuilder()
      const componentDescriptors = await this.contextConfigurationProvider.createComponentDescriptors()
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      await applicationContextBuilder.build(componentDescriptors, this)
    } catch (e: any) {
      Logger.error(`unable to load context ${e.message}`)
      process.exit(1)
    } finally {
      this.registerShutdown()
      this.loaded = true
      Logger.info(`Context loaded in ${(Date.now() - start) / 1000} s`)
    }
  }

  public getActiveProfiles(environment: EnvironmentConfiguration): string[] {
    const profiles = environment.getOrDefault('dino:active:profiles', [])
    return Array.isArray(profiles) ? profiles : profiles.split(',')
  }

  public async destroy(): Promise<void> {
    this.loaded = false
    const cacheValues = Array.from(this.container.cache.values())
    for await (const cacheValue of cacheValues) {
      const target = cacheValue.value
      if (ObjectHelper.instanceOf(target, Component)) {
        await (target as Component).preDestroy()
      }
    }
    await this.container.dispose()
  }

  /**
   * Register the shutdown handler, the handler will be responsible for invoking the preDestroy method
   * on all components and dispose the awilix container.
   */
  public registerShutdown(): void {
    const terminator = (): void => {
      PromiseHelper.handleAsPromiseIfRequired(this.destroy.bind(this)(), () => process.exit(0))
    }

    process.on('SIGINT', terminator)
    process.on('SIGTERM', terminator)
    process.on('SIGUSR1', terminator)
    process.on('SIGUSR2', terminator)
  }
}
