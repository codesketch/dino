//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import fs from 'fs'
import yamljs from 'yamljs'
import camelCase from 'camel-case'
import appRootPath from 'app-root-path'

import { type Injectable } from '../model/injectable'
import { Resolver } from '../model/resolver'
import { Scope } from '../model/scope'
import { Helper } from '../helper/helper'
import { Logger } from '../Logger'
import { ComponentDescriptor } from '../model/ComponentDescriptor'
import { ComponentNameNotProvidedException } from '../exceptions/component.name.not.provided.exception'
import { ComponentNameAlreadyTakenException } from '../exceptions/component.name.already.taken.exception'
import { InjectionMode, type InjectionModeType } from 'awilix'
import { ObjectHelper } from '../helper/object.helper'

/**
 * The provider of the application context configuration.
 * Configuration must have the following yaml format:
 * injectionMode: transient
 * components:
 *  - name: (mandatory) the name of the component, will act as a reference for injection
 *  - source: (mandatory) the source file of the component class relative to the root of the project or a module name
 *  - scope: (optional) the resolution scope for the component, defaults to singleton
 *  - resolver: (optional) the resolver to use available are class, value, function; if omitted class resolver wil be used
 * @typedef {ContextConfigurationProvider}
 * @public
 * @since 0.2.2
 */
export class ContextConfigurationProvider {
  private readonly activeProfiles: string[]
  private readonly contextRoots: string[]
  private readonly conf: any
  private readonly selectedComponentNames: string[]

  /**
   * Constructor for ConfigurationProvider
   * @param {Any} conf the application context configuration
   * @param {array} activeProfiles the profiles that are considered active for the current run
   *
   * @private
   */
  public constructor(conf: any, activeProfiles: string[] = []) {
    // this.validateConfiguration(conf);
    this.conf = conf
    this.activeProfiles = activeProfiles
    this.contextRoots = Helper.getContextRoot(conf)
      .split(',')
      .map((path) => {
        if (path.charAt(0) !== '/') {
          return `${appRootPath.path}/${path}`
        }
        return path
      })
    // this.contextRoot = `${appRootPath.path}/${Helper.getContextRoot(conf)}`;
    // this.componentDescriptors = new Array();
    this.selectedComponentNames = []
  }

  getContextRoots(): string[] {
    return this.contextRoots
  }

  /**
   * Provide the mode the injection will be performed, if mode is not defined "proxy" will be used.
   * Supported injection mode are:
   *  - CLASSIC
   *  - PROXY
   *
   * @returns {awilix.InjectionMode} the injection mode
   * @public
   */
  public mode(): InjectionModeType {
    let answer = InjectionMode.PROXY
    if (this.conf.injectionMode !== undefined && this.conf.injectionMode.toLowerCase() === 'classic') {
      answer = InjectionMode.CLASSIC
    }
    return answer
  }

  /**
   * Normalize the source path
   * @param {String} source the configured source location
   *
   * @private
   */
  public normalizePath(source: string): string {
    const path = `${appRootPath}/${source}`
    return fs.existsSync(`${path}.js`) ? path : source
  }

  /**
   * Create a new instance of ConfigurationProvider
   * @param {String} path the path of the configuration relative to the root of the project,
   * if not provided a scan will be performed to collect all entities defined.
   * @param {array} activeProfiles the profiles that are considered active for the current run
   * @returns {ContextConfigurationProvider}
   *
   * @public
   * @static
   */
  public static create(path?: string, activeProfiles: string[] = []): ContextConfigurationProvider {
    let conf = { injectionMode: 'proxy', contextScan: true, components: [], configurations: [] }
    if (path !== undefined) {
      try {
        Logger.debug(`loading config from path [${path}]`)
        conf = yamljs.load(`${appRootPath}/${path}`)
      } catch (e) {
        Logger.error('unable to load config default to contextScan', e)
      }
    }
    return new ContextConfigurationProvider(conf, activeProfiles)
  }

  /**
   * Create the component descriptors based on the provided configuration or context
   *
   * @returns {Promise<Array<ComponentDescriptor>>} a promise resolved with an array of component descriptors
   * @public
   */
  public async createComponentDescriptors(): Promise<ComponentDescriptor[]> {
    const componentDescriptors: ComponentDescriptor[] = []
    Logger.debug('loading all configurations and components as a component descriptor')
    for (let i = 0; i < (this.conf.configurations ?? []).length; i++) {
      const configuration = this.conf.configurations[i]
      const name: string = camelCase(configuration.name as string)
      this.throwExceptionIfComponentNameIsNotProvidedOrNotAvailable(name)
      Logger.debug(`loading candidate configuration from ${configuration.source}`)
      componentDescriptors.push(
        ComponentDescriptor.create(
          name,
          this.normalizePath(configuration.source as string),
          Helper.asLifetime(configuration.scope as string | Scope | undefined),
          Helper.asResolver(configuration.resolver as string | Resolver | undefined),
          false,
          (configuration.dependsOn as string[]) ?? []
        )
      )
    }

    for (let i = 0; i < (this.conf.components ?? []).length; i++) {
      const component = this.conf.components[i]
      const name = camelCase(component.name as string)
      this.throwExceptionIfComponentNameIsNotProvidedOrNotAvailable(name)
      Logger.debug(`loading candidate component from ${component.source}`)
      componentDescriptors.push(
        ComponentDescriptor.create(
          name,
          this.normalizePath(component.source as string),
          Helper.asLifetime(component.scope as string | Scope | undefined),
          Helper.asResolver(component.resolver as string | Resolver | undefined),
          (component.lazy as boolean) ?? false,
          (component.dependsOn as string[]) ?? []
        )
      )
    }

    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (this.conf.contextScan) {
      Logger.debug('context scan is enabled, loading context based configurations and components as a component descriptor')
      const promises: Array<Promise<void>> = []
      let componentsToLoad: number = 0
      for (let i = 0; i < this.contextRoots.length; i++) {
        const contextRoot = this.contextRoots[i]
        Logger.debug(`>>> inspecting ${contextRoot} for candidates`)
        const sources = Helper.loadSources(contextRoot)
        componentsToLoad += sources.length
        for (const source of sources) {
          promises.push(this.doBuildComponentDescriptorFromSource(source, componentDescriptors))
        }
      }
      const fulfilled = (await Promise.allSettled(promises)).filter((value: PromiseSettledResult<void>) => value.status === 'fulfilled')
      if (fulfilled.length !== componentsToLoad) {
        throw new Error('unable to load all requested dependencies, stopping context loading!')
      }
    }
    return componentDescriptors
  }

  private async doBuildComponentDescriptorFromSource(source: string, componentDescriptors: ComponentDescriptor[]): Promise<void> {
    // for (const source of sources) {
    Logger.debug(`loading candidate component or configuration from ${source}`)
    const injectables: Injectable[] = await Helper.dynamicallyImport(source)
    for (const injectable of injectables) {
      const autoLoad: boolean = ObjectHelper.invoke(injectable, 'autoLoad', true)
      if (!autoLoad) {
        return
      }
      const profile: string = ObjectHelper.invoke(injectable, 'profile')
      const profileIsActive = this.profileIsActive(profile)
      // const isInjectable = Helper.isInjectable(component);
      if (profileIsActive) {
        // const componentDescriptor = this.createComponentDefinitions(component);
        let componentDescriptor: ComponentDescriptor | undefined
        const name = camelCase(injectable.name ?? injectable.constructor.name)
        this.throwExceptionIfComponentNameIsNotProvidedOrNotAvailable(name)
        if (Helper.isConfiguration(injectable)) {
          componentDescriptor = ComponentDescriptor.create(name, injectable, Scope.SINGLETON, Resolver.CLASS, false, [], true)
        }
        if (Helper.isInjectable(injectable)) {
          componentDescriptor = ComponentDescriptor.create(
            name,
            injectable,
            Helper.asLifetime(ObjectHelper.invoke(injectable, 'scope', Scope.SINGLETON) as string | Scope | undefined),
            Resolver.CLASS,
            ObjectHelper.invoke(injectable, 'lazy', false) as boolean,
            ObjectHelper.invoke(injectable, 'dependsOn', []) as string[],
            false
          )
        }
        if (componentDescriptor !== undefined) {
          componentDescriptors.push(componentDescriptor)
        }
      } else {
        Logger.warn(`injectable candidate [${injectable.name}] defined at ${source} not loaded - profile is active ${profileIsActive}`)
      }
    }
  }

  /**
   *
   * @param {String} name the name of the component to validate
   * @private
   */
  private throwExceptionIfComponentNameIsNotProvidedOrNotAvailable(name: string | undefined): void {
    if (name === undefined) {
      throw ComponentNameNotProvidedException.create()
    }
    if (this.selectedComponentNames.includes(name)) {
      throw ComponentNameAlreadyTakenException.create(name)
    }
  }

  private profileIsActive(profile: string): boolean {
    return (
      ObjectHelper.isNotDefined(profile) ||
      (this.activeProfiles !== undefined && this.activeProfiles.length > 0 && this.activeProfiles.includes(profile))
    )
  }
}
