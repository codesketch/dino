import { type AwilixContainer } from 'awilix'
import { type ComponentDescriptor } from '../model/ComponentDescriptor'
import { type ApplicationContext } from './ApplicationContext'

export class ContextScope {
  private readonly scope: AwilixContainer
  private readonly applicationContext: ApplicationContext
  constructor(scope: AwilixContainer, applicationContext: ApplicationContext) {
    this.scope = scope
    this.applicationContext = applicationContext
  }

  register(componentDescriptor: ComponentDescriptor): ContextScope {
    this.scope.register(componentDescriptor.getName(), componentDescriptor.resolve(this.applicationContext))
    return this
  }

  async dispose(): Promise<void> {
    await this.scope.dispose()
  }
}
