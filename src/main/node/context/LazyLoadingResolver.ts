//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import type { ComponentDescriptor } from '../model/ComponentDescriptor'
import type { ApplicationContext } from './ApplicationContext'

export class LazyLoadingResolver implements ProxyHandler<any> {
  private readonly applicationContext: ApplicationContext
  private readonly componentDescriptor: ComponentDescriptor
  private targetRegistered: boolean

  /**
   *
   * @param {ApplicationContext} applicationContext
   * @param {ComponentDescriptor} componentDescriptor
   */
  constructor(applicationContext: ApplicationContext, componentDescriptor: ComponentDescriptor) {
    this.applicationContext = applicationContext
    this.componentDescriptor = componentDescriptor
    this.targetRegistered = false
  }

  get(_target: any, property: string, receiver: any): any {
    if (['postConstruct', 'preDestroy'].includes(property)) {
      // no lifecycle to execute for a proxy...
      return
    }
    return Reflect.get(this.loadInstanceIfRequired(), property, receiver)
  }

  /**
   * Ensure that the component instance is loaded before interacting with it.
   * As the component will be loaded from the application context it will therefore ensure that
   * its lifecycle is executed.
   *
   * @private
   */
  private loadInstanceIfRequired(): any {
    const name = `dinoConcreteInstanceForLazy_${this.componentDescriptor.getName()}`
    if (this.targetRegistered) {
      // Here we resolve to make sure that if lifecycle is transient it is executed
      return this.applicationContext.resolve(name)
    }
    const answer = this.applicationContext.register(name, this.componentDescriptor)
    this.targetRegistered = true
    return answer
  }
}
