//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import appRootPath from 'app-root-path'
import nconf, { type IOptions } from 'nconf'
import { Helper } from '../helper/helper'
import { Logger } from '../Logger'
import { EnvironmentConfiguration } from '../model/environment.configuration'

/**
 * Load the configuration and add all variables to the application context
 * @typedef {EnvironmentConfigurationBuilder}
 * @public
 * @since 0.2.2
 */
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class EnvironmentConfigurationBuilder {
  /**
   * Load the configuration defined for the service. Configuration can be provide as:
   * - command line arguments
   * - environment variables
   * - JSON file, path for the JSON file should be provided
   * @param {String} path the patch for the configuration file relative to the root of the project
   *
   * @public
   * @static
   */
  public static load(): EnvironmentConfiguration {
    // configure nconf to load from command line, environment variable and file.
    const conf = nconf.argv().env()
    let path = conf.get('dino.config.path') ?? conf.get('DINO_CONFIG_PATH')
    if (path !== undefined && path.charAt(0) !== '/') {
      path = `${appRootPath}/${path}`
    }
    Logger.debug(`** loading configuration from ${path}`)
    if (path !== undefined) {
      conf.file({ file: path })
    }
    EnvironmentConfigurationBuilder.loadAdditionalConfigurationLoaders(conf)
    return new EnvironmentConfiguration(conf)
  }

  /**
   * Load additional configuration if required
   *
   * @param {Any} conf the provided configuration
   *
   * @static
   * @private
   */
  private static loadAdditionalConfigurationLoaders(conf: nconf.Provider): void {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const additionalConfigurations = Helper.getVariable(conf, 'dino:configuration', {})
    for (const key in additionalConfigurations) {
      const element = additionalConfigurations[key]
      require(element.module as string)
      nconf.use(key, (element.options ?? {}) as IOptions)
    }
  }
}
