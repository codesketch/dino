//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

export class MethodMissingException extends Error {
  constructor(name = '') {
    super(
      `Method [${name}] has been invoked but has not been found, if you would like more information override the method __call(method, args) in your class`
    )
  }

  public static create(name: string): Error {
    return new MethodMissingException(name)
  }
}
