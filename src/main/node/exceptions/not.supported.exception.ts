//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/**
 * Exception thrown when a resource or the resolution of a resource is not supported
 */
export class NotSupportedException extends Error {
  /**
   * Create a new NotSupportedException, use NotSupportedException#create instead
   *
   * @param {String} name the name of the resource that is not supported
   *
   * @private
   */
  constructor(name = '') {
    super(`Resource [${name}] is not supported`)
  }

  /**
   * Fluent style method to create a new instance of NotSupportedException
   *
   * @param {String} name the name of the resource that is not supported
   * @returns {NotSupportedException}
   *
   * @static
   * @public
   */
  static create(name: string): Error {
    return new NotSupportedException(name)
  }
}
