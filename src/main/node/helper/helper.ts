//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { readdirSync, statSync } from 'fs'
import { type Provider } from 'nconf'
import path, { extname, join } from 'path'
import { Logger } from '../Logger'
import { Injectable } from '../model/injectable'
import { Resolver } from '../model/resolver'
import { Scope } from '../model/scope'
import { ObjectHelper } from './object.helper'

const excludes = ['node_modules', 'coverage', 'eslint', 'test', 'specs', 'nyc_output']
/**
 * Collection of utility functions
 * @typedef Helper
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class Helper {
  private static readonly supportedInjectableTypeNames = ['Injectable', 'Component', 'Service', 'Repository', 'Configuration']

  /**
   * Load all the source files found starting form the provided root directory.
   * @param {String} dir the directly to scan for source files
   * @returns {Generator<String>}
   *
   * @public
   * @static
   */
  public static loadSources(dir: string): string[] {
    Logger.debug(`scanning directory ${dir} for candidate files`)
    if (excludes.some((name) => dir.endsWith(name))) {
      return []
    }
    return readdirSync(dir).reduce((files: string[], file: string) => {
      Logger.debug(`**** found new candidate file - ${file} - for processing`)
      const name = join(dir, file)
      const isDirectory = statSync(name).isDirectory()
      const ext = extname(name)
      Logger.debug(`found file ${file} with extension ${ext} is it a directory ${isDirectory}`)
      if (isDirectory) {
        return [...files, ...Helper.loadSources(name)]
      } else if (ext === '.js') {
        Logger.debug(`found valid source file ${file} with extension ${ext}`)
        return [...files, name]
      }
      return files
    }, [])
  }

  /**
   * Find a source file starting from the requested directory.
   * @param {String} dir the start directory for the search
   * @param {String} name the name of the file source to find
   */
  public static findSource(dir: string, name: string): string {
    const _name = name.endsWith('.js') ? name : `${name}.js`
    const matches = Helper.loadSources(dir).filter((source) => source.includes(_name))
    if (matches.length === 0) {
      // no source found assume the name is a module
      return name
    }
    if (matches.length === 1) {
      return matches[0]
    }
    throw new Error(`found multiple ${matches.join(',')} matches for ${name}`)
  }

  /**
   * Allows to retrieve a variable from the configuration or as environment variable, supports
   * both `:` and `_` notation.
   * @param {Environment} environment the configuration container
   * @param {string} key the key for the variable to retrieve
   * @param {any} _default the default value if the variable is nt defined.
   */
  public static getVariable(environment: Provider, key: string, _default: any): any {
    const envVarKey = key.replace(/[.:]/g, '_')
    const value = [environment.get(envVarKey), environment.get(envVarKey.toUpperCase()), environment.get(key)].find((v) => v !== undefined)

    return value !== undefined ? value : _default
  }

  public static getContextRoot(config: any): string {
    return config.contextRoot ?? process.env.DINO_CONTEXT_ROOT ?? 'src/main/node'
  }

  public static isPath(_string: string): boolean {
    return _string !== undefined && /[<>:"/\\|?*]/.test(_string)
  }

  public static fileNameFromPath(_path: string): string {
    return path.basename(_path).replace('.js', '')
  }

  /**
   * Convert the required scope to Scope
   * @param {String|Scope} scope the scope required for the component
   * @returns {Scope}
   *
   * @static
   * @public
   */
  public static asLifetime(scope: Scope | string | undefined): Scope {
    if (scope === undefined) {
      return Scope.SINGLETON
    }
    if (typeof scope === 'string') {
      // TODO: QB backward compatibility, remove as soon as possible
      return Scope.fromName(scope)
    }
    return scope
  }

  /**
   * Convert the required resolver to Resolver
   * @param {String|Resolver} resolver the resolver required for the component
   * @returns {Resolver}
   *
   * @static
   * @public
   */
  public static asResolver(resolver: Resolver | string | undefined): Resolver {
    if (resolver === undefined) {
      return Resolver.CLASS
    }
    if (typeof resolver === 'string') {
      // TODO: QB backward compatibility, remove as soon as possible
      return Resolver.fromName(resolver)
    }
    return resolver
  }

  public static isInjectable(obj: any): boolean {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (ObjectHelper.instanceOf(obj, Injectable)) {
      return true
    }
    if (!ObjectHelper.isObject(obj) && ObjectHelper.isDefined(obj.prototype?.getTypeName)) {
      return Helper.supportedInjectableTypeNames.includes(obj.prototype.getTypeName() as string)
    }
    if (ObjectHelper.isNotDefined(obj.getTypeName)) {
      return false
    }
    return Helper.supportedInjectableTypeNames.includes(obj.getTypeName() as string)
  }

  public static isConfiguration(obj: Injectable): boolean {
    if (obj.getTypeName === undefined) {
      return false
    }
    return obj.getTypeName() === 'Configuration'
  }

  /**
   * Imports all modules declared as part of the specified path
   * @param {string} path the module path
   * @returns Array<Injectable>
   *
   * @public
   * @static
   */
  public static async dynamicallyImport(path: string): Promise<Injectable[]> {
    try {
      const module: any = await import(path)
      if (module.__esModule === undefined || !ObjectHelper.isDefined(module.default)) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        return Object.keys(module).reduce((answer: Injectable[], key) => {
          const _class = module[key]
          if (Helper.isInjectable(_class)) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            answer.push(_class)
          }
          return answer
        }, [])
      } else {
        return Helper.isInjectable(module) ? [module] : []
      }
    } catch (e: any) {
      Logger.error(`unable to import dependency ${path} because ${e.type} - ${e.message}`)
      throw e
    }
  }
}
