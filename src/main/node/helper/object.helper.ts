import { type PropertyKey } from '../model/proxies/interceptors/abstract.interceptors'

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class ObjectHelper {
  /**
   * Validate if the provided value is defined, not null and not undefined
   * @param {any} value the value to validate
   * @returns true if the value is defined, false otherwise
   * @public
   * @static
   */
  public static isDefined(value: unknown): boolean {
    const defined = value !== null && value !== undefined
    if (typeof value === 'string') {
      return defined && value.length > 0
    }
    return defined
  }

  /**
   * Validate if the provided value is not defined, not null and not undefined
   * @param {any} value the value to validate
   * @returns true if the value is not defined, false otherwise
   * @public
   * @static
   */
  public static isNotDefined(value: unknown): boolean {
    return !ObjectHelper.isDefined(value)
  }

  /**
   * Convert a class to plain object
   * @param {object} clazz the class to be converted to plain object
   * @public
   * @static
   */
  public static classToPlainObject(clazz: any): any {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const entries: Array<[string, any]> = Object.entries(clazz)
    return Object.fromEntries(entries)
  }

  public static isObject(obj: any): boolean {
    return ObjectHelper.isDefined(obj) && typeof obj === 'object'
  }

  /**
   * Evaluate if an obj is an instance of a type
   *
   * @param {Any} obj the object to be verified
   * @param {Any} type the type the object will be verified against
   * @returns {Boolean} true if obj is instance of type, false otherwise
   */
  public static instanceOf(obj: any, type: any): boolean {
    return (
      // eslint-disable-next-line no-prototype-builtins
      Boolean(type.isPrototypeOf(obj)) ||
      obj instanceof type ||
      // eslint-disable-next-line no-prototype-builtins
      (obj.hasOwnProperty('prototype') !== undefined && obj.prototype instanceof type) ||
      // eslint-disable-next-line no-prototype-builtins
      obj.isPrototypeOf(type)
    )
  }

  public static invoke(obj: any, key: PropertyKey, def: any = undefined): any {
    let method = obj[key]
    if (ObjectHelper.isNotDefined(method)) {
      method = obj.prototype[key]
    }
    if (ObjectHelper.isDefined(method)) {
      return method()
    }
    return def
  }
}
