import { ObjectHelper } from './object.helper'

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class PromiseHelper {
  /**
   * Validate that the provided object is a promise or not
   *
   * @param {any} obj the object to be validated as a promise
   *
   * @static
   * @public
   */
  static isAPromise(obj: any): boolean {
    return ObjectHelper.isDefined(obj) && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function'
  }

  /**
   * Allows to wait for a promise to be resolved or invoke the provided directly if the value is not a promise.
   * If the provided value is undefined no action will be taken
   *
   * @param {Promise | any} value the value to check and process
   * @param {Function} callback the callback to invoke when the value is processed,
   * the callback will accept a parameter that is the value or the value resolved from the promise
   * @returns void
   *
   * @static
   * @public
   */
  static handleAsPromiseIfRequired(value: any | Promise<any>, callback: (arg?: any) => void): void {
    if (value !== undefined) {
      if (PromiseHelper.isAPromise(value)) {
        return value.then((cd: any) => {
          callback(cd)
        })
      } else {
        callback(value)
      }
    }
  }
}
