import { type CacheOptions } from './model/cache/cache.manager'
//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { ApplicationContext } from './context/ApplicationContext'
import { ContextScope } from './context/ContextScope'
import { ObjectHelper } from './helper/object.helper'
import { PromiseHelper } from './helper/promise.helper'
import { Logger } from './Logger'
import { CacheContext } from './model/cache/cache.context'
import { Component } from './model/component'
import { ComponentDescriptor } from './model/ComponentDescriptor'
import { Configuration } from './model/configuration'
import { As, Lazy, Profile, Singleton, Transient, type TypeName } from './model/decorators/CommonDecorators'
import { InjectFromConstructor, InjectFromParameter } from './model/decorators/Inject'
import { EnvironmentConfiguration } from './model/environment.configuration'
import { ErrorHandler } from './model/error.handler'
import { ErrorType } from './model/error.type'
import { Injectable } from './model/injectable'
import { Monitor } from './model/monitor/monitor'
import { State } from './model/monitor/state'
import { Repository } from './model/repository'
import { Resolver } from './model/resolver'
import { Scope } from './model/scope'
import { Service } from './model/service'
import { StereotypeProxyHandlerFactory } from './model/stereotype.proxy.handler.factory'
import { Command } from './patterns/Command'
import { type Aggregate } from './patterns/ddd/Aggregate'
import { type Extended } from './patterns/ddd/Archetype'
import { Entity } from './patterns/ddd/Entity'
import { Factory } from './patterns/ddd/Factory'
import { Id } from './patterns/ddd/Id'
import { ValueObject } from './patterns/ddd/ValueObject'
import { Optional } from './patterns/Optional'
import { Translator } from './patterns/Translator'
import { type Nullable } from './Types'

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
class Dino {
  static async run(config?: string): Promise<ApplicationContext> {
    const applicationContext = new ApplicationContext()
    await applicationContext.load(config)
    return applicationContext
  }
}

export {
  ApplicationContext,
  As,
  CacheContext,
  Command,
  Component,
  ComponentDescriptor,
  Configuration,
  ContextScope,
  Dino,
  Entity,
  EnvironmentConfiguration,
  ErrorHandler,
  ErrorType,
  Factory,
  Id,
  Injectable,
  InjectFromConstructor,
  InjectFromParameter,
  Lazy,
  Logger,
  Monitor,
  ObjectHelper,
  // patterns
  Optional,
  Profile,
  PromiseHelper,
  Repository,
  Resolver,
  Scope,
  Service,
  Singleton,
  State,
  StereotypeProxyHandlerFactory,
  Transient,
  Translator,
  ValueObject,
  type Aggregate,
  type CacheOptions,
  type Extended,
  type Nullable,
  type TypeName
}
