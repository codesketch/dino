// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { InvalidValueException } from '../../exceptions/invalid.value.exception'
import { type CacheOptions } from './cache.manager'

export class CacheContext {
  private readonly cacheName: string
  private readonly excludedMethods: string[]
  private readonly cacheOptions: CacheOptions

  /**
   * Create a new cache context
   *
   * @param {String} cacheName the cache name
   * @param {Array} excludedMethods the methods to exclude from caching
   */
  private constructor(cacheName: string, excludedMethods: string[], cacheOptions: CacheOptions) {
    this.cacheName = cacheName
    this.excludedMethods = excludedMethods
    this.cacheOptions = cacheOptions
  }

  public getCacheName(): string {
    return this.cacheName
  }

  public getCacheOptions(): CacheOptions {
    return this.cacheOptions
  }

  /**
   * Determine if the cache is activated for the current method.
   *
   * @param {String} method the method name
   * @returns {Boolean}
   */
  public isCacheActivatedFor(method: string): boolean {
    return !this.excludedMethods.includes(method)
  }

  public static create(
    cacheName: string,
    excludedMethods: string[] = [],
    opts: CacheOptions = { max: 100, maxAge: 300000, updateAgeOnGet: true }
  ): CacheContext {
    if (cacheName === undefined) {
      throw InvalidValueException.create('cache name must be provided')
    }
    return new CacheContext(cacheName, excludedMethods, opts)
  }
}
