// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import LRUCache from 'lru-cache'

export interface CacheOptions {
  max: number
  maxAge: number
  updateAgeOnGet: boolean
}

export class CacheManager {
  private readonly caches: Map<string, LRUCache>

  constructor() {
    this.caches = new Map<string, LRUCache>()
  }

  public store(cacheName: string, key: string, value: any, opts?: CacheOptions): CacheManager {
    this.getCacheByName(cacheName, opts).set(key, value)
    return this
  }

  public get(cacheName: string, key: string, opts?: CacheOptions): any {
    return this.getCacheByName(cacheName, opts).get(key)
  }

  public has(cacheName: string, key: string): boolean {
    return this.getCacheByName(cacheName).has(key)
  }

  public evict(cacheName: string, key: string): CacheManager {
    this.getCacheByName(cacheName).del(key)
    return this
  }

  public clear(cacheName: string): CacheManager {
    this.getCacheByName(cacheName).reset()
    return this
  }

  protected create(cacheName: string, opts: CacheOptions): LRUCache {
    if (!this.caches.has(cacheName)) {
      // const LRUCache = require('lru-cache')
      const lruCache = new LRUCache(opts)
      this.caches.set(cacheName, lruCache)
      return lruCache
    }
    return this.caches.get(cacheName)
  }

  /**
   *
   * @param cacheName the cache name
   * @returns an instance of lru-cache
   */
  protected getCacheByName(cacheName: string, opts: CacheOptions = { max: 100, maxAge: 300000, updateAgeOnGet: true }): LRUCache {
    if (this.caches.has(cacheName)) {
      return this.caches.get(cacheName)
    }
    return this.create(cacheName, opts)
  }
}
