//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

import camelCase from 'camel-case'
import { type ApplicationContext } from '../context/ApplicationContext'
import { ObjectHelper } from '../helper/object.helper'
import { ComponentDescriptor } from './ComponentDescriptor'
import { Injectable } from './injectable'
import { Resolver } from './resolver'
import { Scope } from './scope'

/**
 * A configuration allows to provide component descriptors that describe Injectables that may need additional configuration or that are
 * external to the application core
 *
 * When contextScan is set to true, classes extending this class will be
 * automatically discovered and injected on the application context.
 * @typedef {Configuration}
 *
 * @public
 * @abstract
 */
export abstract class Configuration extends Injectable {
  protected readonly applicationContext: ApplicationContext

  constructor({ applicationContext }) {
    super()
    this.applicationContext = applicationContext
  }

  /**
   * Create a component descriptor to be used to register the value defined on this configuration on the application context.
   *
   * @param {String|Component} value the {String|Component} value to register on the application context
   * @param {Scope} scope the {@link Scope} that the newly registered component should have defaults to {@link Scope.SINGLETON}
   * @param {Resolver} resolver the {@link Resolver} that the newly registered component should have defaults to {@link Resolver.CLASS}
   * @param {String} name the name to assign to the component at resolution time, it will be converted as a camel-case,
   * will be inferred from `value.name` if undefined
   *
   * @returns {ComponentDescriptor} the {@link ComponentDescriptor} to use for resolution
   *
   * @deprecated use any of {ComponentDescriptor#createFromValue} or {ComponentDescriptor#createFromImport} or {ComponentDescriptor#createFromType} methods
   * @public
   */
  asComponentDescriptor<T extends Injectable>(
    value: string | T,
    scope = Scope.SINGLETON,
    resolver = Resolver.CLASS,
    name: string | undefined = undefined
  ): ComponentDescriptor {
    if (ObjectHelper.isNotDefined(name) && ObjectHelper.isNotDefined(value.constructor.name)) {
      throw new Error('unable to determine a name for the component descriptor')
    }
    const _name = camelCase(name ?? value.constructor.name)
    return ComponentDescriptor.create(_name, value, scope, resolver, false, [])
  }

  /**
   * Create a component descriptor to be used to register the value defined on this configuration on the application context.
   *
   * @param {Component} value the value to register on the application context
   * @param {String} scope the scope of the component, can be one of:
   *  - **transient**: a new instance of the component will be created ond injected when required
   *  - **singleton**: a single component instance will be create and reused every time is required
   *  - **scoped**: The registration is scoped to the container - that means that the resolved value
   *
   * @deprecated use {@link Configuration.asComponentDescriptor} as `Configuration.asComponentDescriptor(component, scope, Resolver.VALUE)` instead
   * @public
   *
   */
  asComponentDescriptorWithScope(value: Injectable, scope: Scope): ComponentDescriptor {
    return this.asComponentDescriptor(value, scope, Resolver.VALUE)
  }

  /**
   * Create a component descriptor to be used to register the value defined on this configuration on the application context.
   *
   * @param {Component} value the value to register on the application context
   * @param {String} name the name of the value component to use it will be used as a primary reference, if not provided
   * the framework will try to read the name from the value property of the provided component.
   *
   * @deprecated use {@link Configuration.asComponentDescriptor} as `Configuration.asComponentDescriptor(component, Scope.SINGLETON, Resolver.VALUE, name)` instead
   * @public
   */
  createValueTypeComponentDescriptor(value: Injectable, name: string): ComponentDescriptor {
    return this.asComponentDescriptor(value, Scope.SINGLETON, Resolver.VALUE, name)
  }

  /**
   * Create a component descriptor to be used to register the value defined on this configuration on the application context.
   *
   * @param {Component} value the value to register on the application context
   * @param {String} name the name of the value component to use it will be used as a primary reference, if not provided
   * the framework will try to read the name from the value property of the provided component.
   * @param {String} scope the scope of the component, can be one of:
   *  - **transient**: a new instance of the component will be created ond injected when required
   *  - **singleton**: a single component instance will be create and reused every time is required
   *  - **scoped**: The registration is scoped to the container - that means that the resolved value
   *
   * @deprecated use {@link Configuration.asComponentDescriptor} as `Configuration.asComponentDescriptor(component, scope, Resolver.VALUE, name)` instead
   * @public
   */
  createValueTypeComponentDescriptorWithScope(value: Injectable, name: string, scope: Scope): ComponentDescriptor {
    return this.asComponentDescriptor(value, scope, Resolver.VALUE, name)
  }

  /**
   * Requires a component and register it as part of the application context
   *
   * @param {String} component the path of the component to require
   *
   * @public
   */
  public require(component: string): void {
    this.applicationContext.require(component)
  }

  public getTypeName(): string {
    return 'Configuration'
  }

  /**
   * Allows to provide configuration for specific areas of the applications.
   * the result of the invocation of the configure method will be registered on the
   * application context as-is and made available for injection.
   *
   * **This method must return a ComponentDescriptor, the recommended way to do that is to
   * use the Configuration#asComponentDescriptor method made available on this class.**
   *
   * An example of implementation of the configure function is provided below
   ```javascript
    configure() {
      let myComponent = new MyDbConnectorComponent();
      return this.asComponentDescriptor('myDbComponentDescriptor', myComponent);
    }
   ```
   *
   * @returns {ComponentDescriptor | Promise<ComponentDescriptor>} the object to be set on the configuration
   *
   * @public
   */
  public abstract configure(): ComponentDescriptor | Promise<ComponentDescriptor>
}
