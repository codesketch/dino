// Copyright 2025 Quirino Brizi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { randomUUID } from 'node:crypto'
import { CacheContext } from '../cache/cache.context'
import { type CacheOptions } from '../cache/cache.manager'
import { type Injectable } from '../injectable'
import { Scope } from '../scope'

export const Profile = function (profile: string) {
  return function (target: any) {
    target.profile = () => profile
  }
}

export const Singleton = function () {
  return function (target: any) {
    target.scope = () => Scope.SINGLETON
  }
}

export const Transient = function () {
  return function (target: any) {
    target.scope = () => Scope.TRANSIENT
  }
}

export const Lazy = function () {
  return function (target: any) {
    target.lazy = () => true
  }
}

export const Cacheable = function (
  key: string = randomUUID(),
  excludedMethods: string[] = [],
  opts: CacheOptions = { max: 100, maxAge: 300000, updateAgeOnGet: true }
) {
  return function (target: Injectable) {
    target.cacheable = () => CacheContext.create(key, excludedMethods, opts)
  }
}

export type TypeName = 'Injectable' | 'Component' | 'Service' | 'Repository' | 'Configuration'
export const As = function (typeName: string = 'Injectable') {
  return function (target: any) {
    target.getTypeName = () => typeName
  }
}
