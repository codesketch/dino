import 'reflect-metadata'
import { Logger } from '../../Logger'

const dependencyMap = new Map<string, string[]>()

export const InjectFromConstructor = (dependencies: string[]) => {
  return function (target: object) {
    const className: string | undefined = getName(target)
    if (className !== undefined) {
      dependencyMap.set(className, dependencies)
    }
  }
}

export const InjectFromParameter = function () {
  return function (target: object, propertyName: string): void {
    const className: string | undefined = getName(target)
    if (className !== undefined) {
      let dependencies: string[] | undefined = dependencyMap.get(className)
      if (dependencies === undefined) {
        dependencies = []
        dependencyMap.set(className, dependencies)
      }
      dependencies.push(propertyName)
    }
  }
}

export const getInjectableDependencies = function (target: any): string[] {
  const className: string | undefined = getName(target)
  if (className !== undefined) {
    const dependencies: string[] = dependencyMap.get(className) ?? []
    Logger.debug(`Found ${dependencies.join(', ')} injectable dependencies for ${className}`)
    return dependencies
  }
  return []
}

const getName = (target: any): string | undefined => {
  return target.name ?? target.constructor?.name
}
