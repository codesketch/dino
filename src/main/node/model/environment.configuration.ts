// Copyright 2023 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { type Provider } from 'nconf'
import { Helper } from '../helper/helper'

/**
 * @class
 * @public
 * @type EnvironmentConfiguration
 */
export class EnvironmentConfiguration {
  private readonly nconf: Provider
  /**
   *
   * @param {nconf} nconf the nconf object
   * @constructor
   */
  constructor(nconf: Provider) {
    this.nconf = nconf
  }

  /**
   * Allow to retrieve a value for a particular key
   * @param {string} key the to retrieve
   * @returns the value associate with the provided key or undefined
   *
   * @public
   */
  public get(key: string): any {
    if (!key.includes(':')) {
      // the key might have been passed with . notation instead of :
      key = key.replace('.', ':')
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    return Helper.getVariable(this.nconf, key, undefined)
  }

  /**
   * Allows to retrieve a value or a provided default value
   * @param {string} key the key to retrieve
   * @param {any} def the default value
   * @returns The value associated with the provided key of the default value
   *
   * @public
   */
  public getOrDefault(key: string, def: any): any {
    if (!key.includes(':')) {
      // the key might have been passed with . notation instead of :
      key = key.replace('.', ':')
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    return Helper.getVariable(this.nconf, key, def)
  }
}
