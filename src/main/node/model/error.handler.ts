//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Logger } from '../Logger'
import { ErrorType } from './error.type'
import { Injectable } from './injectable'

/**
 * An error handler will be registered and invoked in case of an uncought error or unhandled promise
 *
 * When contextScan is set to true, classes extending this class will be
 * automatically discovered and injected on the application context.
 * @typedef {ErrorHandler}
 *
 * @public
 * @abstract
 *
 * @class ErrorHandler
 * @since 0.4.7
 */
export abstract class ErrorHandler extends Injectable {
  /**
   * Lifecycle hook allows to execute initialization steps for a component behaviour.
   *
   * @public
   */
  postConstruct(): void {
    process.on('uncaughtException', (error: Error) => {
      this.handle(error, ErrorType.UNCAUGHT_ERROR)
    })
    process.on('unhandledRejection', (error: Error) => {
      this.handle(error, ErrorType.UNCAUGHT_PROMISE)
    })
  }

  /**
   * Lifecycle hook allows to execute initialization steps for a component behaviour.
   *
   * @public
   */
  preDestroy(): void {}

  /**
   * Handler for the uncaught error, in its basic form the method is empty and
   * just logs the received error.
   * Extend it to include your logic
   * @param {Error} error the error that has been caught
   * @param {ErrorType} type the type of error that has been caught
   *
   * @public
   */
  handle(error: Error, type: string | ErrorType): void {
    Logger.error(`${type} - ${error.message}`)
  }

  getTypeName(): string {
    return 'ErrorHandler'
  }
}
