export enum ErrorType {
  UNCAUGHT_ERROR = 'uncaughtError',
  UNCAUGHT_PROMISE = 'uncaughtPromiseError'
}
