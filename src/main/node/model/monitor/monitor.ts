//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Component } from '../component'
import { type State } from './state'

/**
 * A monitor represent a component that responsible to provide access to
 * insight about the running application.
 *
 * When contextScan is set to true, classes extending this class will be
 * automatically discovered and injected on the application context.
 *
 * Concrete Monitor classes should implement the Monitor#execute() method
 * and return a State object.
 *
 * @public
 */
export class Monitor extends Component {
  /**
   * Define the name of the monitoring group/properties
   */
  public getName(): string {
    return 'default'
  }

  /**
   * Extract information about the state of a service or component.
   * @returns {State} the state of the service.
   */
  public execute(): State {
    throw new Error('Not implemented yet')
  }
}
