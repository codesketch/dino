//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/**
 * A value object responsible to carry information about the runtime of a service or component.
 *
 * @typedef State
 * @public
 */
export class State {
  private readonly properties: Map<string, any>
  constructor() {
    this.properties = new Map()
  }

  /**
   * Allows to access the internal property Map
   * @returns {Map} the properties Map
   */
  public getProperties(): Map<string, any> {
    return this.properties
  }

  /**
   * Allows to add a property to the internal Map.
   * @param {String} name the name of the property
   * @param {Any} value the value associated with the property
   * @returns {State} this object
   */
  public setProperty(name: string, value: any): State {
    this.properties.set(name, value)
    return this
  }

  public static create(): State {
    return new State()
  }

  /**
   * Convenient method to create a State with the provided properties.
   * @param {Map<String, Any>} properties the properties to add to thi state
   */
  public static from(properties): State {
    const state = new State()
    ;(properties ?? new Map()).forEach((value: any, key: string) => state.setProperty(key, value))
    return state
  }

  public static measures(): object {
    return Object.freeze({ uptime: 'uptime', state: 'state' })
  }
}
