//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Logger } from '../../Logger'
import { MethodMissingException } from '../../exceptions/method.missing.exception'
import { type Injectable } from '../injectable'
import { type AbstractInterceptor } from './interceptors/abstract.interceptors'
import { CacheInterceptor } from './interceptors/cache.interceptor'
import { DefaultInterceptor } from './interceptors/default.interceptor'

/**
 * Create a new basic proxy which will access properties or methods of the target object and will
 * invoke a specific method on the target object if the method or property is no part of the target object.
 */
export class DefaultProxyHandler implements ProxyHandler<any> {
  private readonly excludedProperties = ['debug.description', 'nodejs.util.inspect.custom', 'prototype']

  /**
   * Implement the get method for the {@link Proxy} class
   *
   * @param {Injectable} obj the target object
   * @param {String} prop the property to access
   * @returns {any} The value of the accessed property or the result of the function execution
   */
  public get(obj: Injectable, prop: string): any {
    if (Reflect.has(obj, prop)) {
      const interceptor: AbstractInterceptor | undefined = this.interceptors().find((interceptor) => interceptor.isApplicable(obj, prop))
      if (interceptor === undefined) {
        return
      }
      return interceptor.intercept(obj, prop)
    } else {
      if (!this.excludedProperties.includes(prop)) {
        Logger.debug(
          `Method [${String(
            prop
          )}] has been invoked but has not been found, if you would like more information override the method __call(method, args) in your class`
        )
        if (Reflect.has(obj, '__call')) {
          // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
          return (...args: any) => obj.__call(prop, args)
        } else {
          throw MethodMissingException.create(`${obj.constructor.name}#${prop}`)
        }
      }
    }
  }

  protected interceptors(): AbstractInterceptor[] {
    return [CacheInterceptor.create(), DefaultInterceptor.create()]
  }
}
