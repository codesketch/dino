// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { NotSupportedException } from '../../../exceptions/not.supported.exception'

export type PropertyKey = string | number | symbol

export abstract class AbstractInterceptor {
  /**
   * Intercept the execution executing logic before and/or after the method execution.
   *
   * @param {Injectable} obj the injectable object
   * @param {String} prop the name of the method or property that is accessed via this proxy
   * @returns {Function} a function accepting the incoming argument that is able to execute the required logic
   * The firm for the returned function would be:
   * ```javascript
   * (...args) => {}
   * ```
   */
  public intercept(_obj: any, _prop: PropertyKey): any {
    throw NotSupportedException.create('AbstractInterceptor#intercept')
  }

  /**
   * Validate if the current interceptor is applicable
   *
   * @param {Any} obj the current object target of the proxy
   * @param {String} prop the method or property name currently invoked
   */
  isApplicable(_obj: any, _prop: PropertyKey): boolean {
    throw NotSupportedException.create('AbstractInterceptor#isApplicable')
  }
}
