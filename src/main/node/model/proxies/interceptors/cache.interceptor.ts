// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { PromiseHelper } from '../../../helper/promise.helper'
import { type CacheContext } from '../../cache/cache.context'
import { CacheKeyGenerator } from '../../cache/cache.key.generator'
import { CacheManager } from '../../cache/cache.manager'
import { type Injectable } from '../../injectable'
import { AbstractInterceptor } from './abstract.interceptors'

/**
 * Implement an "around" interceptor that provide caching functionalities
 */
export class CacheInterceptor extends AbstractInterceptor {
  private static _instance: CacheInterceptor
  private readonly cacheManager: CacheManager
  private readonly cacheKeyGenerator: CacheKeyGenerator
  constructor() {
    super()
    this.cacheManager = new CacheManager()
    this.cacheKeyGenerator = CacheKeyGenerator.create()
  }

  public intercept(obj: Injectable, prop: string): any {
    const cacheContext: CacheContext | null | undefined = obj.cacheable !== undefined ? obj.cacheable() : null
    return (...args) => {
      let answer = this.get(cacheContext, ...args)
      if (answer == null || answer === undefined) {
        // no cache element found proceed on the chain to retrieve information
        answer = obj[prop].bind(obj)(...args)
        if (PromiseHelper.isAPromise(answer)) {
          return answer.then((data: any) => this.set(cacheContext, data, ...args))
        } else {
          this.set(cacheContext, answer, ...args)
        }
      }
      return answer
    }
  }

  public isApplicable(obj: Injectable, prop: string): boolean {
    const cacheContext: CacheContext | null | undefined = obj.cacheable !== undefined ? obj.cacheable() : undefined
    if (typeof obj[prop] !== 'function') {
      return false
    }
    if (cacheContext == null) {
      return false
    }
    return cacheContext?.isCacheActivatedFor(prop)
  }

  public get(cacheContext: CacheContext | null | undefined, ...args: any[]): any {
    if (cacheContext == null) {
      return null
    }
    const cacheName = cacheContext.getCacheName()
    const key = this.cacheKeyGenerator.generate(...args)
    return this.cacheManager.get(cacheName, key, cacheContext.getCacheOptions())
  }

  public set(cacheContext: CacheContext | null | undefined, data: any, ...args: any[]): any {
    if (cacheContext == null) {
      return data
    }
    const cacheName = cacheContext.getCacheName()
    const key = this.cacheKeyGenerator.generate(...args)
    this.cacheManager.store(cacheName, key, data, cacheContext.getCacheOptions())
    return data
  }

  public static create(): CacheInterceptor {
    if (CacheInterceptor._instance === undefined) {
      CacheInterceptor._instance = new CacheInterceptor()
    }
    return CacheInterceptor._instance
  }
}
