// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { AbstractInterceptor, type PropertyKey } from './abstract.interceptors'

export class DefaultInterceptor extends AbstractInterceptor {
  private static _instance: DefaultInterceptor

  public intercept(obj: any, prop: PropertyKey): unknown {
    return Reflect.get(obj, prop)
  }

  public isApplicable(_obj: any, _prop: PropertyKey): boolean {
    return true
  }

  public static create(): DefaultInterceptor {
    if (DefaultInterceptor._instance === undefined) {
      DefaultInterceptor._instance = new DefaultInterceptor()
    }
    return DefaultInterceptor._instance
  }
}
