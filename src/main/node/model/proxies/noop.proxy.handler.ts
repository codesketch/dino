//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { DefaultProxyHandler } from './default.proxy.handler'
import { type AbstractInterceptor } from './interceptors/abstract.interceptors'
import { DefaultInterceptor } from './interceptors/default.interceptor'

/**
 * Create a new basic proxy which will access properties or methods of the target object and will
 * invoke a specific method on the target object if the method or property is no part of the target object.
 */
export class NoOpProxyHandler extends DefaultProxyHandler {
  protected interceptors(): AbstractInterceptor[] {
    return [DefaultInterceptor.create()]
  }
}
