//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Component } from './component'

/**
 * A repository represent a component that is responsibility is to access in a
 * collection like data required by the application.
 *
 * When contextScan is set to true, classes extending this class will be
 * automatically discovered and injected on the application context.
 *
 * As of now Repositories are just stereotypes that define responsibilities plan is to
 * extend the behaviour and add transactional nature to the methods defined on the
 * implementing classes.
 *
 * @typedef Repository
 * @public
 */
export class Repository extends Component {
  public getTypeName(): string {
    return 'Repository'
  }

  // -- Transactions

  // -- EXECUTION

  /**
   * Execution method that allows to initiate the resources required for the transactional
   * environment
   *
   * @protected
   */
  protected beginTransaction(): void {}

  /**
   * Execution method that allows to complete the initiated transaction
   *
   * @protected
   */
  protected completeTransaction(): void {}

  /**
   * Execution method that allows to rollback the initiated transaction
   *
   * @protected
   */
  protected rollbackTransaction(): void {}

  /**
   * Execution method that allows to clean-up resources used for the transaction
   *
   * @protected
   */
  protected cleanupTransactionResources(): void {}

  // -- CONFIGURATION

  /**
   * Defines if an injectable is transactional or not. By default this method checks if any
   * methods have been defined to require a transaction, in this case returns true, otherwise false.
   *
   * @returns {Boolean} true if the injectable is transactional, false otherwise
   *
   * @public
   */
  public transactional(): boolean {
    return this.enableTransactionOn().length > 0
  }

  /**
   * Allows to define the methods that should be treated as requiring transactions.
   *
   * @returns {Array<String>} an array containing the names of the methods that should
   * be wrapped in a transaction
   *
   * @public
   */
  public enableTransactionOn(): string[] {
    return []
  }
}
