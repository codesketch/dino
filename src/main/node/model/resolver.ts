//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import {
  type BuildResolver,
  type Constructor,
  type DisposableResolver,
  type FunctionReturning,
  asClass,
  asFunction,
  asValue,
  type Resolver as AWResolver
} from 'awilix'

export type ResolvableInjectable<T> = Constructor<T> & T & FunctionReturning<T>
export type ResolvedInjectable<T> = AWResolver<T> | (BuildResolver<T> & DisposableResolver<T>)
type Primitive = typeof asClass | typeof asValue | typeof asFunction

export class Resolver {
  public static CLASS = new Resolver('CLASS', asClass)
  public static VALUE = new Resolver('VALUE', asValue)
  public static FUNCTION = new Resolver('FUNCTION', asFunction)

  private readonly id: string
  private readonly primitive: Primitive

  private constructor(id: string, primitive: Primitive) {
    this.id = id
    this.primitive = primitive
  }

  public getId(): string {
    return this.id
  }

  public isClassOrFunction(): boolean {
    return this.id === 'CLASS' || this.id === 'FUNCTION'
  }

  public asPrimitive(): Primitive {
    return this.primitive
  }

  /**
   * Wrap the target component to an awilix resolver.
   * @param {object} target the target component to wrap
   * @returns {any} the awilix resolver
   */
  public wrap(target: ResolvableInjectable<any>): ResolvedInjectable<any> {
    return this.primitive(target)
  }

  /**
   * Lookup a resolver from it name
   * @param {String} name the name of the resolver to lookup
   *
   * @returns {Scope} the {@link Resolver} retrieved from it name
   *
   * @default Scope.SINGLETON
   *
   * @public
   */
  public static fromName(name: string): Resolver {
    switch (name.toUpperCase()) {
      case 'VALUE':
        return new Resolver('VALUE', asValue)
      case 'FUNCTION':
        return new Resolver('FUNCTION', asFunction)
      default:
        return new Resolver('CLASS', asClass)
    }
  }
}
