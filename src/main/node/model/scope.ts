//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

type Lifetime = 'SINGLETON' | 'TRANSIENT' | 'SCOPED'

/**
 * Define the possible scopes for an entity
 * @public
 * @class
 * @public
 * @type Scope
 */
export class Scope {
  /**
   * A single instance of the entity will be injected for all dependencies
   */
  public static SINGLETON = new Scope('SINGLETON', 'SINGLETON')
  /**
   * A new instance of the entity is returned every time is requested
   */
  public static TRANSIENT = new Scope('TRANSIENT', 'TRANSIENT')
  /**
   * A single instance per scope is available
   */
  public static SCOPED = new Scope('SCOPED', 'SCOPED')

  private readonly id: string
  private readonly primitive: Lifetime

  constructor(id: string, primitive: Lifetime) {
    this.id = id
    this.primitive = primitive
  }

  public getId(): string {
    return this.id
  }

  /**
   * Convert the current scope to awilix Lifetime primitive
   *
   * @returns {awilix.Lifetime}
   * @public
   */
  public asPrimitive(): Lifetime {
    return this.primitive
  }

  /**
   * Lookup a scope from it name
   * @param {String} name the name of the scope to lookup
   *
   * @returns {Scope} the {@link Scope} retrieved from it name
   *
   * @default Scope.SINGLETON
   *
   * @public
   */
  public static fromName(name: string): Scope {
    switch (name.toUpperCase()) {
      case 'TRANSIENT':
        return new Scope('TRANSIENT', 'TRANSIENT')
      case 'SCOPED':
        return new Scope('SCOPED', 'SCOPED')
      default:
        return new Scope('SINGLETON', 'SINGLETON')
    }
  }
}
