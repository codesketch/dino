//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

import { Component } from './component'

/**
 * A service represent a component that is responsibility is to orchestrate
 * logic across domain objects.
 * When contextScan is set to true, classes extending this class will be
 * automatically discovered and injected on the application context.
 *
 * @typedef Service
 * @public
 */
export class Service extends Component {
  public getTypeName(): string {
    return 'Service'
  }
}
