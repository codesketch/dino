//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { NotSupportedException } from '../exceptions/not.supported.exception'
import { ObjectHelper } from '../helper/object.helper'
import { DefaultProxyHandler } from './proxies/default.proxy.handler'
import { NoOpProxyHandler } from './proxies/noop.proxy.handler'
import { ServiceProxyHandler } from './proxies/service.proxy.handler'
import { TransactionalProxyHandler } from './proxies/transactional.proxy.handler'

/**
 * Creates a factory able to create different types of proxies that will wrap
 * a specific stereotype providing tools to control different aspects of the
 * code execution lifecycle
 */
export class StereotypeProxyHandlerFactory {
  private readonly proxies: Map<string, DefaultProxyHandler>
  private static instance: StereotypeProxyHandlerFactory

  constructor() {
    this.proxies = new Map<string, DefaultProxyHandler>()
    this.proxies.set('Injectable', new DefaultProxyHandler())
    this.proxies.set('Configuration', new NoOpProxyHandler())
    this.proxies.set('ErrorHandler', new DefaultProxyHandler())
    this.proxies.set('Component', new DefaultProxyHandler())
    this.proxies.set('Service', new ServiceProxyHandler())
    this.proxies.set('Repository', new TransactionalProxyHandler())
  }

  /**
   * Allows to get a proxy specifically built for the requested injectable.
   * @param {Injectable} injectable the injectable type the proxy should be constructed for
   *
   * @returns {Proxy}
   * @public
   */
  public getProxyFor(injectable: string): DefaultProxyHandler {
    const proxy: DefaultProxyHandler | undefined = this.proxies.get(injectable)
    if (proxy !== undefined) {
      return proxy
    }
    throw NotSupportedException.create(injectable)
  }

  public static getInstance(): StereotypeProxyHandlerFactory {
    if (!ObjectHelper.isDefined(StereotypeProxyHandlerFactory.instance)) {
      StereotypeProxyHandlerFactory.instance = new StereotypeProxyHandlerFactory()
    }
    return StereotypeProxyHandlerFactory.instance
  }
}
