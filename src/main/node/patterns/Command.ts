import { type ApplicationContext } from '../context/ApplicationContext'
import { Component } from '../main'
import { type EnvironmentConfiguration } from '../model/environment.configuration'

interface Deps {
  environment: EnvironmentConfiguration
  applicationContext: ApplicationContext
}

/**
 * Provide a basic implementation of the behavioural Command pattern.
 *
 * Both EnvironmentConfiguration and ApplicationContext are automatically made
 * available to concrete classes
 */
export abstract class Command<I, R> extends Component {
  protected readonly environment: EnvironmentConfiguration
  protected readonly applicationContext: ApplicationContext

  constructor({ environment, applicationContext }: Deps) {
    super()
    this.environment = environment
    this.applicationContext = applicationContext
  }

  abstract execute(input: I | undefined): Promise<R>
}
