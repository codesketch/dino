import { ObjectHelper } from '../helper/object.helper'

/**
 * Implements the Optional patters
 * @public
 * @class
 */
export class Optional<T> {
  private readonly value: T

  /**
   * Create a new instance of optional
   * @param value the value set as optional
   * @private
   */
  constructor(value: T) {
    this.value = value
  }

  /**
   * Allows to access this optional value
   * @returns this optional value
   * @public
   */
  public get(): T {
    return this.value
  }

  /**
   * Validate is this optional value is present
   * @returns true if this optional contains a not null and not undefined value, false otherwise
   * @public
   */
  public isPresent(): boolean {
    return ObjectHelper.isDefined(this.value)
  }

  /**
   * Create an optional starting from the provided value.
   * @param value the value to set on this optional. The value can be undefined or null
   * @returns a new Optional instance
   * @public
   * @static
   */
  public static from<T>(value: T): Optional<T> {
    return new Optional(value)
  }
}
