import { Component } from '../model/component'
export abstract class Translator<P, R> extends Component {
  public abstract translate(p: P): R
}
