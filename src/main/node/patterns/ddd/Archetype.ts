export type Props = Record<string, any>
export type Extended<B, E> = B & E

export const wrapWithPropertyGetterAndSetterHandler = <T extends object>(target: T): T => {
  const handler: ProxyHandler<T> = {
    get: (target: T, prop: string | symbol) => target[prop],
    set: (target: T, prop: string | symbol, newValue: any) => (target[prop] = newValue)
  }
  return new Proxy(target, handler)
}

export const wrapWithPropertyAccessorHandler = <T extends object>(target: T): T => {
  const handler: ProxyHandler<T> = {
    get: (target: T, prop: string | symbol) => target[prop],
    set: (target: T, prop: string | symbol, newValue: any) => (target[prop] = newValue)
  }
  return new Proxy(target, handler)
}

export interface Archetype {
  /**
   * Compare two archetypes for equality
   * @param other the other archetype to compare
   * @returns true if the two archetypes are same, false otherwise
   */
  equals: <T extends Archetype>(other: T) => boolean
}
