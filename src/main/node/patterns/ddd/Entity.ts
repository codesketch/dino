import { ObjectHelper } from '../../helper/object.helper'
import { wrapWithPropertyGetterAndSetterHandler, type Archetype, type Props } from './Archetype'
import { type Id } from './Id'

/**
 * Define an entity in the common sense of DDD (Domain-Driven Design).
 * The properties passed to the constructor will be set with the provided value
 * and will only be accessible for reading and writing.
 */
export class Entity<T, P extends Props = Record<string, any>> implements Archetype {
  private readonly id: Id<T>

  protected constructor(id: Id<T>, props?: P) {
    this.id = id
    for (const key in props) {
      Object.defineProperty(this, key, {
        value: props[key],
        writable: true,
        enumerable: true,
        configurable: true
      })
    }
  }

  public getId(): Id<T> {
    return this.id
  }

  /**
   * Compare two entities for equality
   * > if other object is not defined return false
   * > if this object and other object are not of the same type then return false
   * > if the id of this object properties is not same as the id of other object then return false
   * > otherwise return true
   * @param other the other archetype to compare
   * @returns true if the two entities are same, false otherwise
   */
  public equals<T extends Archetype>(other: T): boolean {
    if (!ObjectHelper.isDefined(other)) {
      return false
    }
    if (!ObjectHelper.instanceOf(other, Entity)) {
      return false
    }
    return this.id.equals((other as unknown as Entity<any, any>).id)
  }

  public static create<T, P extends Props = Record<string, any>>(id: Id<T>, props?: P): Entity<T, P> {
    return wrapWithPropertyGetterAndSetterHandler<Entity<T, P>>(new this(id, props))
  }
}
