import { NotSupportedException } from '../../exceptions/not.supported.exception'
import { Component } from '../../model/component'
import { type ApplicationContext } from '../../context/ApplicationContext'

interface Deps {
  applicationContext: ApplicationContext
}

/**
 * Helps create complex objects and aggregates
 */
export abstract class Factory<I, T> extends Component {
  protected readonly applicationContext: ApplicationContext
  constructor({ applicationContext }: Deps) {
    super()
    this.applicationContext = applicationContext
  }

  /**
   * Create an new instance based on the input information and internal logic
   * @param i the input for the factory
   */
  public abstract create(i: I): T
  /**
   * Create an new instance based on the input information and internal logic
   * @param i the input for the factory
   * @deprecated use Factory#create instead, **this method was introduced by error and will be removed soon**
   */
  public translate(_i: I): T {
    throw NotSupportedException.create('use Factory#create instead')
  }
}
