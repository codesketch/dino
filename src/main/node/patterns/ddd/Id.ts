import { ObjectHelper } from '../../helper/object.helper'
import { type Archetype } from './Archetype'

/**
 * Represent an identifier for an entity
 */
export class Id<T = string> {
  private readonly value: T

  protected constructor(value: T) {
    this.value = value
  }

  /**
   * Compare two id for equality
   * > if other object is not defined return false
   * > if this object and other object are not of the same type then return false
   * > if any of this object properties is not same as the same property on other object then return false
   * > otherwise return true
   * @param other the other archetype to compare
   * @returns true if the two Ids are same, false otherwise
   */
  public equals<T extends Archetype>(other: T): boolean {
    if (!ObjectHelper.isDefined(other)) {
      return false
    }
    if (!ObjectHelper.instanceOf(other, Id)) {
      return false
    }
    if (typeof this.value === 'string') {
      return this.value.toLowerCase() === (other as unknown as Id<any>).value.toLowerCase()
    }
    return this.value === (other as unknown as Id<any>).value
  }

  /**
   * Allows to access this identifier internal value
   * @returns the identifier internal value
   */
  public get(): T {
    return this.value
  }

  /**
   * Create a new identifier
   * @param value the Id internal value
   * @returns a new Id instance
   */
  public static create<T>(value: T): Id<T> {
    return new Id(value)
  }
}
