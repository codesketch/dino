import { ObjectHelper } from '../../main'
import { wrapWithPropertyAccessorHandler, type Archetype, type Props } from './Archetype'

/**
 * Define a value object in the common sense of DDD (Domain-Driven Design).
 * The properties passed to the constructor will be set with the provided value
 * and will only be accessible for reading.
 */
export class ValueObject<T extends Props = Record<string, any>> implements Archetype {
  [x: string]: any

  protected constructor(props?: T) {
    const ps = props ?? {}
    for (const key in ps) {
      Object.defineProperty(this, key, {
        value: ps[key],
        writable: false,
        enumerable: true,
        configurable: true
      })
    }
  }

  /**
   * Compare two value objects for equality, the basic implementation is as following:
   * > if other object is not defined return false
   * > if this object and other object are not of the same type then return false
   * > if any of this object properties is not same as the same property on other object then return false
   * > otherwise return true
   * @param other the other archetype to compare
   * @returns true if the two value objects are same, false otherwise
   */
  public equals<T extends Archetype>(other: T): boolean {
    if (!ObjectHelper.isDefined(other)) {
      return false
    }
    if (!ObjectHelper.instanceOf(other, this.constructor)) {
      return false
    }
    for (const key of Object.getOwnPropertyNames(this)) {
      if (this[key] !== (other as any)[key]) {
        return false
      }
    }
    return true
  }

  public static create<P extends Props = Record<string, any>, Return extends ValueObject = ValueObject>(props?: P): Return {
    // eslint-disable-next-line no-new
    return wrapWithPropertyAccessorHandler<Return>(new this(props) as Return)
  }
}
