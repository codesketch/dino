//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { ApplicationContextHelper } from '../../../main/node/context/ApplicationContextHelper'
import { describe, it } from 'mocha'

describe.skip('Application Context Helper', () => {
  const applicationContextHelper = new ApplicationContextHelper()

  it('builds a dependency tree', () => {
    // const componentDescriptors: ComponentDescriptor[] = new Array();
    // componentDescriptors.push(ComponentDescriptor.create('component1', '', Scope.SINGLETON, Resolver.CLASS, false, []));
    // componentDescriptors.push(ComponentDescriptor.create('component2', '', Scope.SINGLETON, Resolver.CLASS, false, ['applicationContext']));
    // componentDescriptors.push(ComponentDescriptor.create('component3', '', Scope.SINGLETON, Resolver.CLASS, false, ['component1']));
    // componentDescriptors.push(ComponentDescriptor.create('component4', '', Scope.SINGLETON, Resolver.CLASS, false, ['component2']));
    // componentDescriptors.push(ComponentDescriptor.create('component5', '', Scope.SINGLETON, Resolver.CLASS, false, ['component1', 'component2']));
    // componentDescriptors.push(ComponentDescriptor.create('component6', '', Scope.SINGLETON, Resolver.CLASS, false, ['component1', 'component5']));
    // // act
    // const actual = applicationContextHelper.buildDependencyTree(componentDescriptors);
    // if (actual === undefined) {
    //   assert.fail();
    // }
    // // expect
    // expect((actual.get(Symbol.for('0')) ?? []).map((cd) => cd.getName())).to.be.eql(['component1', 'component2']);
    // expect((actual.get(Symbol.for('1')) ?? []).map((cd) => cd.getName())).to.be.eql(['component3', 'component4', 'component5']);
    // expect((actual.get(Symbol.for('2')) ?? []).map((cd) => cd.getName())).to.be.eql(['component6']);
  })
})
