//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { assert } from 'chai'
import { Component } from '../../../main/node/model/component'
import { ApplicationContext } from '../../../main/node/context/ApplicationContext'
import { before, describe } from 'mocha'

describe.skip('Application Context', () => {
  const applicationContext = new ApplicationContext()

  // act
  // process.env.DINO_CONTEXT_ROOT = 'integration';
  before(async () => {
    process.env.DINO_CONFIG_PATH = 'src/test/resources/config.json'
    await applicationContext.load('src/test/resources/dino.yml')
  })

  it('load the application context', () => {
    // assert
    assert.notEqual(applicationContext.resolve('testComponent'), null)
  })

  it('lazy loads components', () => {
    // prepare
    const testComponent = applicationContext.resolve('testComponent')
    // act
    const actual = testComponent.get('test lazy')
    // assert
    assert.ok(actual == 'test lazy')
    // act
    const actual1 = testComponent.get('new test lazy')
    // assert
    assert.ok(actual1 == 'new test lazy')
  })

  it('propagate an instance of application context for injection', () => {
    // assert
    assert.equal(applicationContext.resolve('applicationContext'), applicationContext)
  })

  it('propagate an instance of configuration environment', () => {
    let environment = applicationContext.resolve('environment')
    // assert
    assert.notEqual(environment, null)
    assert.equal(environment.get('DINO_CONFIG_PATH'), 'src/test/resources/config.json')
    assert.equal(environment.get('test'), 'value for test 1')
  })

  it('return all instances of a type', () => {
    // act
    let actual = applicationContext.resolveAll(Component)
    // assert
    assert.ok(actual.length > 1)
  })

  it('require the component if not defined on the application context', () => {
    let actual = applicationContext.require('util')
    assert.ok(actual != undefined)
  })
})
