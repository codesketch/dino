//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import appRootPath from 'app-root-path'
import assert from 'assert'

import { ContextConfigurationProvider } from '../../../main/node/context/ContextConfigurationProvider'
import { InjectionMode, Lifetime } from 'awilix'
import { it } from 'mocha'

describe('Context Configuration Provider', () => {
  let testObj = ContextConfigurationProvider.create('src/test/resources/dino.yml', [])

  it('loads injection mode', () => {
    assert.equal(testObj.mode(), InjectionMode.PROXY)
  })

  it('loads components', async () => {
    // act
    let components = await testObj.createComponentDescriptors()
    assert.equal(components.length, 3)
    assert.equal(components[0].getName(), 'testComponent')
    assert.equal(components[0].getScope().asPrimitive(), Lifetime.TRANSIENT)
  })

  it('loads components with defaults', async () => {
    // act
    let components = await testObj.createComponentDescriptors()
    assert.equal(components.length, 3)
    assert.equal(components[1].getName(), 'testComponent1')
    assert.equal(components[1].getScope().asPrimitive(), Lifetime.SINGLETON)
  })

  it('loads modules', async () => {
    // act
    let components = await testObj.createComponentDescriptors()
    assert.equal(components.length, 3)
    assert.equal(components[2].getName(), 'fs')
    assert.equal(components[2].getScope().asPrimitive(), Lifetime.SINGLETON)
    assert.equal(components[2].getName(), 'fs')
  })

  it.skip('loads component sources', async () => {
    process.env.DINO_CONTEXT_ROOT = 'examples/contextscan/components'
    let testObj = ContextConfigurationProvider.create()
    // act
    let components = await testObj.createComponentDescriptors()
    process.env.DINO_CONTEXT_ROOT = ''
    // assert
    assert.ok(components.length >= 1)
    assert.ok(!components.find((cmp) => cmp.getName() == 'testProfileComponent'))
  })

  it.skip('loads component sources with profile', async () => {
    process.env.DINO_CONTEXT_ROOT = 'examples/contextscan/components'
    let testObj = ContextConfigurationProvider.create(undefined, ['test'])
    // act
    let components = await testObj.createComponentDescriptors()
    process.env.DINO_CONTEXT_ROOT = ''
    // assert
    assert.ok(components.length >= 1)
    assert.ok(!!components.find((cmp) => cmp.getName() == 'testProfileComponent'))
  })

  it('throws exception if component name is not provided', () => {
    // act
    try {
      ContextConfigurationProvider.create('../resources/dino-no-component-name.yml')
    } catch (error) {
      assert.equal(error.message, 'missing name property on component')
    }
  })

  it('throws exception if component source is not provided', () => {
    // act
    try {
      ContextConfigurationProvider.create('../resources/dino-no-component-source.yml')
    } catch (error) {
      assert.equal(error.message, 'missing source property on component')
    }
  })

  it('throws exception if component name are duplicated', () => {
    // act
    try {
      ContextConfigurationProvider.create('../resources/dino-duplicated-component-name.yml')
    } catch (error) {
      assert.equal(error.message, 'duplicated component name - testComponent')
    }
  })

  it.skip('loads component from multiple path', async () => {
    process.env.DINO_CONTEXT_ROOT = 'examples/contextscan/components,integration'
    let testObj = ContextConfigurationProvider.create(undefined, ['test'])
    // act
    let components = await testObj.createComponentDescriptors()
    process.env.DINO_CONTEXT_ROOT = ''
    // assert
    assert.ok(components.length >= 1)
    assert.ok(!!components.find((cmp) => cmp.getName() == 'testProfileComponent'))
    assert.ok(!!components.find((cmp) => cmp.getName() == 'testOrderComponent'))
  })
})
