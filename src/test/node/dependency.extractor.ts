// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { expect } from 'chai';
import { DependencyExtractor } from '../../main/node/DependencyExtractor';

class ClassWithDependenciesAsProxy {
  // eslint-disable-next-line no-unused-vars
  constructor({ dependencyOne, dependencyTwo }) {}
}

class ClassWithOneDependencyAsProxy {
  // eslint-disable-next-line no-unused-vars
  constructor({ dependencyOne }) {}
}

class ClassWithOneDependency {
  // eslint-disable-next-line no-unused-vars
  constructor(dependencyOne) {}
}

class ClassWithTwoDependencies {
  // eslint-disable-next-line no-unused-vars
  constructor(dependencyOne, dependencyTwo) {}
}

class ClassWithNoDependencies {
  constructor() {}
}

class ClassWithNoConstructor {}

class ClassWithNoConstructorThatExtends extends ClassWithDependenciesAsProxy {}
class ClassWithConstructorThatExtends extends ClassWithDependenciesAsProxy {
  // eslint-disable-next-line no-unused-vars
  constructor(dependencyZero) {
    super(dependencyZero);
  }
}

describe('DependencyExtractor', () => {
  const testObj = new DependencyExtractor();

  it('extract dependencies for class w/multiple dependencies', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithDependenciesAsProxy);
    // assert
    expect(dependencies.length).to.be.eql(2);
    expect(dependencies[0]).to.be.eql('dependencyOne');
    expect(dependencies[1]).to.be.eql('dependencyTwo');
  });

  it('extract dependencies for class w/one dependencies', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithOneDependencyAsProxy);
    // assert
    expect(dependencies.length).to.be.eql(1);
    expect(dependencies[0]).to.be.eql('dependencyOne');
  });

  it('extract dependencies for class w/multiple dependencies no proxy', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithTwoDependencies);
    // assert
    expect(dependencies.length).to.be.eql(2);
    expect(dependencies[0]).to.be.eql('dependencyOne');
    expect(dependencies[1]).to.be.eql('dependencyTwo');
  });

  it('extract dependencies for class w/one dependencies no proxy', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithOneDependency);
    // assert
    expect(dependencies.length).to.be.eql(1);
    expect(dependencies[0]).to.be.eql('dependencyOne');
  });

  it('extract dependencies for class w/one dependencies no proxy', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithNoDependencies);
    // assert
    expect(dependencies.length).to.be.eql(0);
  });

  it('extract dependencies for class w/no constructor', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithNoConstructor);
    // assert
    expect(dependencies.length).to.be.eql(0);
  });

  it('extract dependencies for extended class first no constructor', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithNoConstructorThatExtends);
    // assert
    expect(dependencies.length).to.be.eql(2);
    expect(dependencies[0]).to.be.eql('dependencyOne');
    expect(dependencies[1]).to.be.eql('dependencyTwo');
  });

  it('extract dependencies for extended class first w/constructor', () => {
    // act
    const dependencies = testObj.extractDependenciesFor(ClassWithConstructorThatExtends);
    // assert
    expect(dependencies.length).to.be.eql(3);
    expect(dependencies[0]).to.be.eql('dependencyZero');
    expect(dependencies[1]).to.be.eql('dependencyOne');
    expect(dependencies[2]).to.be.eql('dependencyTwo');
  });
});
