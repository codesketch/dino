//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Helper } from '../../../main/node/helper/helper';
import { Component } from '../../../main/node/model/component';

import assert from 'assert';
import appRootPath from 'app-root-path';

describe('Helper', () => {
  it('loads all source files', () => {
    let found = 0;
    for (let source of Helper.loadSources(`${appRootPath}/src/test/resources/js`)) {
      assert.ok(source.endsWith('.js'));
      found++;
    }
    assert.notEqual(found, 0);
  });

  it('loads requested source', () => {
    let actual = Helper.findSource(`${appRootPath}/src/test/resources/js`, 'logger');
    assert.ok(actual.endsWith('.js'));
    assert.ok(actual.includes(appRootPath));
  });

  it('loads requested source from nested directory', () => {
    let actual = Helper.findSource(`${appRootPath}/src/test/resources/js`, 'component');
    assert.ok(actual.endsWith('.js'));
    assert.ok(actual.includes(appRootPath));
  });

  it('returns the module name if no source is found', () => {
    let actual = Helper.findSource(`${appRootPath}/src`, 'fs');
    assert.ok(!actual.endsWith('.js'));
    assert.ok(!actual.includes(appRootPath));
  });

  it('validate the provided name is path', () => {
    assert.ok(Helper.isPath('/name'));
  });

  it('validate the provided name is not path', () => {
    assert.ok(!Helper.isPath('name'));
  });

  it('returns the filename w/extension', () => {
    assert.equal('aws.cloud.request.handler', Helper.fileNameFromPath('/handlers/aws.cloud.request.handler.js'));
  });

  it('returns the filename', () => {
    assert.equal('aws.cloud.request.handler', Helper.fileNameFromPath('/handlers/aws.cloud.request.handler'));
  });

  it('validate if a component is injectable', () => {
    class Test extends Component {}
    assert.ok(Helper.isInjectable(Test));
  });
});
