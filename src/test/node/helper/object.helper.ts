import { expect } from 'chai';
import { ObjectHelper } from '../../../main/node/helper/object.helper';

class Test {
  private a;
  private b;
  constructor(a, b) {
    this.a = a;
    this.b = b;
  }

  getA() {
    return this.a;
  }

  getB() {
    return this.b;
  }
}

describe('Object Helper', () => {
  it('convert a class to plain object excluding methods', () => {
    const test = new Test('a1', 'b1');
    // act
    const actual = ObjectHelper.classToPlainObject(test);
    // assert
    expect(Array.from(Object.keys(actual)).includes('getA')).to.be.false;
    expect(Array.from(Object.keys(actual)).includes('getB')).to.be.false;
    expect(actual.a).to.be.eq('a1');
    expect(actual.b).to.be.eq('b1');
  });

  it('convert a class to plain object excluding methods recursively', () => {
    const test = new Test(new Test('a2', 'b2'), 'b1');
    // act
    const actual = ObjectHelper.classToPlainObject(test);
    // assert
    expect(Object.keys(actual).includes('getA')).to.be.false;
    expect(Object.keys(actual).includes('getB')).to.be.false;
    expect(actual.a).to.be.eql({
      a: 'a2',
      b: 'b2',
    });
    expect(actual.b).to.be.eq('b1');
  });
});
