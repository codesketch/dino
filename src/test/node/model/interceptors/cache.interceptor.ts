// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { CacheContext } from '../../../../main/node/model/cache/cache.context';
import { CacheInterceptor } from '../../../../main/node/model/proxies/interceptors/cache.interceptor';

import { expect } from 'chai';
import Sinon from 'sinon';

describe('Cache Interceptor', () => {
  const testObj = CacheInterceptor.create();

  it('retrieve from cache', () => {
    // prepare
    const cacheEnabledInjectable = Sinon.spy(new CacheEnabledInjectable());
    // act
    testObj.intercept(cacheEnabledInjectable, 'get')('12345', 'abcd');
    testObj.intercept(cacheEnabledInjectable, 'get')('12345', 'abcd');
    // assert
    expect(cacheEnabledInjectable.get.calledOnceWith('12345', 'abcd')).to.be.true;
  });

  it('does not retrieve from cache', () => {
    // prepare
    const cacheEnabledInjectable = Sinon.spy(new CacheDisabledInjectable());
    // act
    testObj.intercept(cacheEnabledInjectable, 'get')('12345', 'abcd');
    testObj.intercept(cacheEnabledInjectable, 'get')('12345', 'abcd');
    // assert
    expect(cacheEnabledInjectable.get.calledTwice).to.be.true;
    expect(cacheEnabledInjectable.get.calledWith('12345', 'abcd')).to.be.true;
  });
});

class CacheEnabledInjectable {
  get(id) {
    return id;
  }

  cacheable() {
    return CacheContext.create('cache');
  }
}

class CacheDisabledInjectable {
  get(id) {
    return id;
  }

  cacheable() {
    return null;
  }
}
