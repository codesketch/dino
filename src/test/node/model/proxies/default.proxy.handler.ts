//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { expect } from 'chai';
import { MethodMissingException } from '../../../../main/node/exceptions/method.missing.exception';
import { CacheContext } from '../../../../main/node/model/cache/cache.context';
import { Injectable } from '../../../../main/node/model/injectable';
import { DefaultProxyHandler } from '../../../../main/node/model/proxies/default.proxy.handler';
import Sinon from 'sinon';

describe('DefaultProxyHandler', () => {
  it('access valid method', () => {
    class Target extends Injectable {
      method(arg) {
        return arg;
      }
    }
    const proxy = new Proxy<Target>(new Target(), new DefaultProxyHandler());
    // act
    const actual = proxy.method('called - 1');
    // assert
    expect(actual).to.be.eq('called - 1');
  });

  it('access valid method', () => {
    class Target extends Injectable {
      method(arg) {
        return arg;
      }
    }
    const proxy = new Proxy(new Target(), new DefaultProxyHandler());
    // act
    expect(() => proxy['nonExistent']('called - 1')).to.throw(MethodMissingException);
  });

  it('does not use interceptor if not required', () => {
    class CacheDisabledMethodInjectable {
      get(id) {
        return id;
      }

      getById(id) {
        return id;
      }

      cacheable() {
        return CacheContext.create('cache', ['getById']);
      }
    }
    const cacheEnabledInjectable = Sinon.spy(new CacheDisabledMethodInjectable());
    const testObj = new Proxy(cacheEnabledInjectable, new DefaultProxyHandler());

    // act
    testObj.get('12345');
    testObj.get('12345');
    testObj.getById('12345');
    testObj.getById('12345');

    // assert
    expect(cacheEnabledInjectable.get.calledOnceWith('12345')).to.be.true;
    expect(cacheEnabledInjectable.getById.calledTwice).to.be.true;
    expect(cacheEnabledInjectable.getById.calledWith('12345')).to.be.true;
  });
});
