//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { expect } from 'chai';
import { TransactionalProxyHandler } from '../../../../main/node/model/proxies/transactional.proxy.handler';

import Sinon from 'sinon';

class TransactionalRepo {
  get() {}
  save() {}
  saveWithError() {
    throw Error('saveWithError');
  }

  async saveWithErrorAsync() {
    throw Error('saveWithErrorAsync');
  }

  saveWithErrorPromise() {
    return Promise.reject(new Error('saveWithErrorPromise'));
  }

  beginTransaction() {}
  completeTransaction() {}
  rollbackTransaction() {}
  cleanupTransactionResources() {}

  transactional() {
    return this.enableTransactionOn().length > 0;
  }

  enableTransactionOn() {
    return ['save', 'saveWithError', 'saveWithErrorAsync', 'saveWithErrorPromise'];
  }

  cacheable() {}
}

describe('Transactional Proxy Handler', () => {
  let testObj;
  let transactionalRepo;
  beforeEach(() => {
    transactionalRepo = Sinon.spy(new TransactionalRepo());
    testObj = new Proxy(transactionalRepo, new TransactionalProxyHandler());
  });

  it('does not invoke transaction methods for unwanted methods', () => {
    // actual
    testObj.get();
    // assert
    expect(transactionalRepo.get.calledOnce).to.be.true;
    expect(transactionalRepo.beginTransaction.notCalled).to.be.true;
    expect(transactionalRepo.completeTransaction.notCalled).to.be.true;
    expect(transactionalRepo.rollbackTransaction.notCalled).to.be.true;
    expect(transactionalRepo.cleanupTransactionResources.notCalled).to.be.true;
  });

  it('does invoke transaction methods for configured  methods', () => {
    // actual
    testObj.save();
    // assert
    expect(transactionalRepo.get.notCalled).to.be.true;
    expect(transactionalRepo.save.calledOnce).to.be.true;
    expect(transactionalRepo.beginTransaction.calledOnce).to.be.true;
    expect(transactionalRepo.completeTransaction.calledOnce).to.be.true;
    expect(transactionalRepo.rollbackTransaction.notCalled).to.be.true;
    expect(transactionalRepo.cleanupTransactionResources.calledOnce).to.be.true;
  });

  it('does invoke transaction rollback on error for configured  methods', () => {
    // actual
    try {
      testObj.saveWithError();
    } catch (e) {
      expect(e.message).to.be.eql('saveWithError');
    }
    // assert
    expect(transactionalRepo.get.notCalled).to.be.true;
    expect(transactionalRepo.saveWithError.calledOnce).to.be.true;
    expect(transactionalRepo.beginTransaction.calledOnce).to.be.true;
    expect(transactionalRepo.completeTransaction.notCalled).to.be.true;
    expect(transactionalRepo.rollbackTransaction.calledOnce).to.be.true;
    expect(transactionalRepo.cleanupTransactionResources.calledOnce).to.be.true;
  });

  it('does invoke transaction rollback on error for configured async methods', async () => {
    // actual
    try {
      await testObj.saveWithErrorAsync();
    } catch (e) {
      expect(e.message).to.be.eql('saveWithErrorAsync');
    }
    // assert
    expect(transactionalRepo.get.notCalled).to.be.true;
    expect(transactionalRepo.saveWithErrorAsync.calledOnce).to.be.true;
    expect(transactionalRepo.beginTransaction.calledOnce, 'should have called beginTransaction').to.be.true;
    expect(transactionalRepo.completeTransaction.notCalled, 'should not have called completeTransaction').to.be.true;
    expect(transactionalRepo.rollbackTransaction.calledOnce, 'should have called rollbackTransaction').to.be.true;
    expect(transactionalRepo.cleanupTransactionResources.calledOnce, 'should have called cleanupTransactionResources').to.be.true;
  });

  it('does invoke transaction rollback on error for configured promise-based methods', async () => {
    // actual
    try {
      await testObj.saveWithErrorPromise();
    } catch (e) {
      expect(e.message).to.be.eql('saveWithErrorPromise');
    }
    // assert
    expect(transactionalRepo.get.notCalled).to.be.true;
    expect(transactionalRepo.saveWithErrorPromise.calledOnce).to.be.true;
    expect(transactionalRepo.beginTransaction.calledOnce, 'should have called beginTransaction').to.be.true;
    expect(transactionalRepo.completeTransaction.notCalled, 'should not have called completeTransaction').to.be.true;
    expect(transactionalRepo.rollbackTransaction.calledOnce, 'should have called rollbackTransaction').to.be.true;
    expect(transactionalRepo.cleanupTransactionResources.calledOnce, 'should have called cleanupTransactionResources').to.be.true;
  });
});
