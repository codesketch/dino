//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import assert from 'assert';
import { Scope } from '../../../main/node/model/scope';

describe('Scope', () => {
  it('lookup a scope from its name', () => {
    const actual = Scope.fromName('transient');
    // assert
    assert.strictEqual(actual.getId(), Scope.TRANSIENT.getId());
  });

  it('default to singlethon if name is not known', () => {
    const actual = Scope.fromName('unknown');
    // assert
    assert.strictEqual(actual.getId(), Scope.SINGLETON.getId());
  });
});
