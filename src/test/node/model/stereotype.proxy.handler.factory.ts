//    Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { expect } from 'chai';
import { NotSupportedException } from '../../../main/node/exceptions/not.supported.exception';
import { DefaultProxyHandler } from '../../../main/node/model/proxies/default.proxy.handler';
import { StereotypeProxyHandlerFactory } from '../../../main/node/model/stereotype.proxy.handler.factory';

describe('StereotypeProxyHandlerFactory', () => {
  it('retrieves a valid proxy', () => {
    // act
    const proxy = StereotypeProxyHandlerFactory.getInstance().getProxyFor('Injectable');
    // assert
    expect(proxy).not.undefined;
    expect(proxy).to.be.instanceof(DefaultProxyHandler);
  });

  it('throws exception for invalid proxy', () => {
    // act
    expect(() => StereotypeProxyHandlerFactory.getInstance().getProxyFor('Foo')).to.throws(NotSupportedException);
  });
});
