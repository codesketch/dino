import { expect } from 'chai'
import { describe, it } from 'mocha'
import { Entity, Id } from '../../../../main/node/main'

class TestEntity extends Entity<string> {}

describe('Entity', () => {
  it('defines properties', () => {
    const testObj = TestEntity.create(Id.create('11'), { prop1: 'value' })

    expect(testObj.getId().get()).to.be.eq('11')
    expect(testObj.prop1).to.be.eq('value')
  })

  it('allows to redefines properties', () => {
    const testObj = TestEntity.create(Id.create('11'), { prop1: 'value' })

    expect(testObj.getId().get()).to.be.eq('11')
    expect(testObj.prop1).to.be.eq('value')

    testObj.prop1 = 'value3'

    expect(testObj.prop1).to.be.eq('value3')
  })
})
