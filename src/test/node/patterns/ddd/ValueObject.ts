import { expect } from 'chai'
import { describe, it } from 'mocha'
import { ValueObject } from '../../../../main/node/main'

class TestValueObject extends ValueObject {
  public prop1: string | undefined
}

describe('ValueObject', () => {
  it('check equality', () => {
    const testObj = TestValueObject.create({ prop1: 'value', prop2: 'value2' })
    const testObj2 = TestValueObject.create({ prop1: 'value', prop2: 'value2' })

    expect(testObj.equals(testObj2)).to.be.true
  })

  it('check equality negative', () => {
    const testObj = TestValueObject.create({ prop1: 'value', prop2: 'value2' })
    const testObj2 = TestValueObject.create({ prop1: 'value', prop2: 'value3' })
    expect(testObj.equals(testObj2)).to.be.false
  })

  it('check properties are not writable', () => {
    const testObj = TestValueObject.create({ prop1: 'value', prop2: 'value2' })
    try {
      testObj.prop1 = 'value3'
    } catch (e) {
      expect(e.message).to.be.eql("Cannot assign to read only property 'prop1' of object '#<TestValueObject>'")
    }
  })

  it('check properties are set', () => {
    const testObj = TestValueObject.create({ prop1: 'value', prop2: 'value2' })
    expect(testObj.prop1).to.be.eql('value')
  })

  it('check properties are set from variable', () => {
    const info: any = { prop1: 'value', prop2: 'value2' }
    const testObj = TestValueObject.create(info)
    expect(testObj.prop1).to.be.eql('value')
    expect(testObj.prop2).to.be.eql('value2')
  })

  it('test s dynamic type', () => {
    class Metadata extends ValueObject {
      myProp: string
    }
    const t: Metadata = Metadata.create({ myProp: 'abcd' })
    expect(t.myProp).to.be.eql('abcd')
  })
})
