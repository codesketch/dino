import { Component } from '../../main/node/model/component';

export class TestComponent extends Component {
  get(a) {
    return a;
  }

  dependsOn() {
    return ['a', 'b'];
  }
}
